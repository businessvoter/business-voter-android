package com.businessvoter.ui.address

import androidx.lifecycle.MutableLiveData
import com.businessvoter.R
import com.businessvoter.model.LoginInfo
import com.businessvoter.model.UserInfo
import com.businessvoter.ui.base.BaseViewModel
import com.businessvoter.util.extentions.bindString
import com.businessvoter.util.extentions.get
import com.businessvoter.util.validator.EthAddressValidator

class EnterTokenContractViewModel : BaseViewModel<EnterTokenContractNavigator>() {
    var userInfo: UserInfo? = null
    val tokenContractAddress: MutableLiveData<String> = MutableLiveData("")

    fun onContinueClick() {
        try {
            val tokenContractAddress: String = tokenContractAddress.get()
            val tokenAddressError: String = EthAddressValidator.validate(tokenContractAddress)
            if (tokenAddressError.isNotBlank()) {
                throw Exception(tokenAddressError)
            }
            val userInfo = userInfo ?: throw Error()
            val loginInfo = LoginInfo(
                userInfo.address,
                userInfo.signature,
                tokenContractAddress,
                userInfo.privateKey
            )
            navigator?.goToEnterPin(loginInfo)
        } catch (e: Exception) {
            navigator?.showError(
                e.localizedMessage ?: bindString(R.string.common_error_invalid_address)
            )
        }
    }
}