package com.businessvoter.repository

import com.businessvoter.api.Api
import com.businessvoter.db.dao.AnswerDao
import com.businessvoter.db.dao.PollDao
import com.businessvoter.helper.runNetwork
import com.businessvoter.model.LoginInfo
import com.businessvoter.model.api.UserLoginPostRequest
import com.businessvoter.model.api.UserLoginPostResponse
import com.businessvoter.model.api.UserResponse
import com.businessvoter.model.db.Answer
import com.businessvoter.model.db.Poll
import com.businessvoter.prefs.UserPrefs

object UserRepository {

    suspend fun getUserInfo() {
        val response: UserResponse = runNetwork {
            Api.api.getUserInfo()
        }
        saveUserInfo(response)
    }

    suspend fun postUserInfo(loginInfo: LoginInfo) {
        val response: UserLoginPostResponse = runNetwork {
            Api.api.loginUser(
                UserLoginPostRequest(
                    address = loginInfo.address,
                    signature = loginInfo.signature,
                    tokenContract = loginInfo.tokenAddress
                )
            )
        }
        UserPrefs.accessToken = response.token
        UserPrefs.privateKey = loginInfo.privateKey
        saveUserInfo(response.user)
    }

    private fun saveUserInfo(userResponse: UserResponse) {
        UserPrefs.setUserInfo(userResponse)
        UserPrefs.setTokenInfo(userResponse.token)
        userResponse.polls.forEach {
            PollDao.db.insertPoll(Poll(it))
            AnswerDao.db.insertAnswers(it.answers.map { answer -> Answer(it.id, answer) })
        }
    }

}