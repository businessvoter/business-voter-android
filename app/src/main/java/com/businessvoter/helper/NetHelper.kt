package com.businessvoter.helper

import com.businessvoter.api.ApiException
import com.businessvoter.api.ERROR_CODE_NO_INTERNET
import com.businessvoter.api.ERROR_CODE_REQUEST_TIMEOUT
import com.businessvoter.api.ERROR_CODE_UNKNOWN
import com.businessvoter.prefs.UserPrefs
import kotlinx.coroutines.Deferred
import kotlinx.coroutines.sync.Mutex
import kotlinx.coroutines.sync.withLock
import retrofit2.HttpException
import java.io.IOException
import java.net.SocketTimeoutException
import java.net.UnknownHostException
import java.util.concurrent.atomic.AtomicBoolean

val isTokenOnSync: AtomicBoolean = AtomicBoolean(false)
private val mutex = Mutex()

@Throws(ApiException::class)
suspend inline fun <reified R> runNetwork(call: () -> Deferred<R>): R {
//    while (isTokenOnSync.get()) {
//        delay(100)
//    }
    return try {
        call().await()
    } catch (e: Throwable) {
        val apiException: ApiException = handleException(e)
        throw apiException
//        if ((apiException.code == ERROR_CODE_USER_UNAUTHORIZED || apiException.code == ERROR_CODE_TOKEN_EXPIRED) && (R::class != AccessToken::class)) {
//            runTokenRequest()
//            try {
//                call().await()
//            } catch (e: Throwable) {
//                throw handleException(e)
//            }
//        } else {
//
//
//            throw apiException
//        }
    }
}

@Throws(ApiException::class)
suspend fun runTokenRequest(): String = mutex.withLock {
    try {
        if (isTokenOnSync.getAndSet(true)) return@withLock ""
        loadTokenSync()
        isTokenOnSync.set(false)
        UserPrefs.accessToken
    } catch (e: Throwable) {
        isTokenOnSync.set(false)
        throw handleException(e)
    }
}

fun loadTokenSync() {
//    val call: Call<AccessToken> = if (!EncryptionHelper.hasKey()) {
//        Api.api.getAnonymousToken()
//    } else {
//        val rootKey: ByteArray = BipGeneratorHelper.generateMasterSeedFromWordsSync(EncryptionHelper.seedWords).bytes
//        val keyHash: String = Hex.toHexString(HashUtil.sha3(HashUtil.sha256(rootKey).bytes))
//        rootKey.forEachIndexed { index, _ -> rootKey[index] = 0 }
//        Api.api.getAccountToken(hash = keyHash)
//    }
//    val response: retrofit2.Response<AccessToken> = try {
//        call.execute()
//    } catch (e: Exception) {
//        throw handleException(e)
//    }
//
//    val body: AccessToken? = response.body()
//    if (response.isSuccessful && body != null) {
//        UserPrefs.accessToken = body.accessToken
//    } else {
//        throw ApiException(response)
//    }
}

fun handleException(e: Throwable): ApiException = when (e) {
    is ApiException -> e
    is HttpException -> ApiException(e.response())
    is UnknownHostException -> ApiException(ERROR_CODE_NO_INTERNET, e)
    is IOException -> ApiException(ERROR_CODE_NO_INTERNET, e)
    is SocketTimeoutException -> ApiException(ERROR_CODE_REQUEST_TIMEOUT, e)
    is RuntimeException -> ApiException(ERROR_CODE_UNKNOWN, e)
    else -> ApiException(e)
}