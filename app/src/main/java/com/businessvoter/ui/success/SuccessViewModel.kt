package com.businessvoter.ui.success

import androidx.lifecycle.MutableLiveData
import com.businessvoter.R
import com.businessvoter.ui.base.BaseViewModel
import com.businessvoter.util.extentions.bindString
import com.businessvoter.util.extentions.get

class SuccessViewModel : BaseViewModel<SuccessNavigator>() {
    val successText: MutableLiveData<String> = MutableLiveData()
    val txId: MutableLiveData<String> = MutableLiveData()

    fun onDetailsClick() {
        navigator?.navigateToWeb(bindString(R.string.common_block_explorer_eth, txId.get()))
    }
}