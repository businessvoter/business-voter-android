package com.businessvoter.model.ui

import com.businessvoter.R
import com.businessvoter.model.db.Answer
import com.businessvoter.model.db.Poll
import com.businessvoter.ui.base.DiffCompared
import com.businessvoter.util.extentions.bindString
import com.businessvoter.util.extentions.msToSec
import com.businessvoter.util.extentions.secToMs
import java.text.SimpleDateFormat
import java.util.*

data class PollUI(
    val pollId: Long,
    val question: String,
    val answer: String,
    val userStaked: String,
    val endTime: Long
) : DiffCompared {
    constructor(poll: Poll) : this(
        poll.id,
        poll.text,
        "",
        "0",
        poll.endTime
    )

    constructor(poll: Poll, answer: Answer) : this(
        poll.id,
        poll.text,
        answer.text,
        answer.userStaked,
        poll.endTime
    )

    private fun isFinished(): Boolean = endTime < System.currentTimeMillis().msToSec()

    fun getStatusText(): String =
        bindString(
            if (isFinished()) R.string.main_vote_poll_finished else R.string.main_vote_poll_in_progress
        )

    fun getTextColor(): Int =
        if (isFinished()) R.color.statusFinished else R.color.statusInProgress

    fun getFormattedEndTime(): String =
        SimpleDateFormat(
            "dd.MM.yyyy 'at' HH:mm:ss",
            Locale.getDefault()
        ).format(Date(endTime.secToMs()))

    override fun idEquals(obj: Any): Boolean = (obj is PollUI)
            && obj.pollId == this.pollId

    override fun uiEquals(obj: Any): Boolean {
        val that = obj as? PollUI ?: return false
        if (!this.idEquals(obj)) return false
        if (that.question != this.question) return false
        if (that.answer != this.answer) return false
        if (that.endTime != this.endTime) return false
        return this.userStaked == that.userStaked
    }
}