package com.businessvoter.ui.details

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.businessvoter.BuildConfig
import com.businessvoter.R
import com.businessvoter.api.Api
import com.businessvoter.db.dao.AnswerDao
import com.businessvoter.db.dao.PollDao
import com.businessvoter.model.api.EthPostTransactionRequestBody
import com.businessvoter.model.db.Answer
import com.businessvoter.model.db.Poll
import com.businessvoter.prefs.UserPrefs
import com.businessvoter.ui.base.BaseViewModel
import com.businessvoter.util.crypto.DataActionEncoder
import com.businessvoter.util.extentions.*
import com.businessvoter.util.formatAmount
import com.businessvoter.util.toBigDecimalSafe
import com.businessvoter.util.view.OrderedVerticalAnswerItem
import com.paytomat.eth.utils.Numeric
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import org.bouncycastle.util.encoders.Hex
import org.web3j.crypto.Credentials
import org.web3j.crypto.ECKeyPair
import org.web3j.crypto.RawTransaction
import org.web3j.crypto.TransactionEncoder
import java.math.BigDecimal
import java.math.BigInteger
import java.text.SimpleDateFormat
import java.util.*

class PollDetailsViewModel : BaseViewModel<PollDetailsNavigator>() {
    val progressMode: ProgressModeLiveData = ProgressModeLiveData()
    val pollId: MutableLiveData<Long> = MutableLiveData()

    private val poll: LiveData<Poll> = pollId.flatMap {
        PollDao.db.getPollById(it)
    }

    val question: LiveData<String> = poll.map {
        it.text
    }

    val formattedEndDate: LiveData<String> = poll.map {
        bindString(
            R.string.poll_details_end_date,
            SimpleDateFormat(
                "dd.MM.yyyy 'at' HH:mm:ss",
                Locale.getDefault()
            ).format(Date(it.endTime.secToMs()))
        )
    }

    private val answers: LiveData<List<Answer>> = poll.flatMap {
        AnswerDao.db.getAnswersByPollId(it.id)
    }

    val totalStaked: LiveData<String> = answers.map {
        val totalStaked: BigDecimal = it.fold(BigDecimal.ZERO) { acc, answer ->
            acc + answer.totalStaked.toBigDecimalSafe()
        }
        bindString(
            R.string.poll_details_total_staked,
            formatAmount(totalStaked.toString())
        )
    }

    val isTotalStakedVisible: LiveData<Boolean> = poll.map {
        it.isFinished()
    }

    val isUnstakeVisible: LiveData<Boolean> = poll.map {
        it.isFinished() && it.userStaked.toBigDecimalSafe() > BigDecimal.ZERO
    }

    val models: LiveData<List<OrderedVerticalAnswerItem>> =
        poll.zip(answers).map { (poll, answers) ->
            val totalVotes: BigDecimal = answers.fold(BigDecimal.ZERO) { acc, answer ->
                acc + answer.totalStaked.toBigDecimalSafe()
            }
            val maxValue: BigDecimal =
                answers.maxBy { it.totalStaked.toBigDecimalSafe() }?.totalStaked.toBigDecimalSafe()
            answers.map { answer ->
                OrderedVerticalAnswerItem(
                    answer,
                    { onAnswerSelected(answer.id) },
                    if (totalVotes > BigDecimal.ZERO) answer.totalStaked.toBigDecimalSafe()
                        .div(totalVotes).toInt() * 100 else 0,
                    poll.isFinished(),
                    answer.totalStaked.toBigDecimalSafe() == maxValue
                )
            }
        }

    fun onUnstakeClick() {
        navigator?.goToConfirmPin(bindString(R.string.poll_details_unstake_confirm_text))
    }

    fun onUnstakeConfirmed() {
        val poll: Poll = poll.value ?: return
        progressMode.progress()
        JobBuilder {
            val privateKey: ByteArray = Hex.decode(UserPrefs.privateKey)
            val credentials: Credentials = Credentials.create(ECKeyPair.create(privateKey))
            val nonce: BigInteger = BigInteger.valueOf(Api.api.getNonce().await().nonce)
            val gasLimit: BigInteger = BigInteger.valueOf(800000)
            val gasPrice: BigInteger =
                BigInteger.valueOf(Api.api.getGasPrice().await().gasPrice.toLong())
            val chainId: Byte = 3

            val data = DataActionEncoder.encodeUnstakeData(
                BuildConfig.SMART_CONTRACT,
                BigInteger.valueOf(poll.id)
            )

            val rawTransaction: RawTransaction =
                RawTransaction.createTransaction(
                    nonce,
                    gasPrice,
                    gasLimit,
                    BuildConfig.SMART_CONTRACT,
                    BigInteger.ZERO,
                    data
                )
            val signedMessage: ByteArray =
                TransactionEncoder.signMessage(rawTransaction, chainId, credentials)

            val hexMessage: String = Numeric.toHexString(signedMessage)
            val txId: String = Api.api.postTransaction(
                EthPostTransactionRequestBody(
                    hexMessage.substring(2)
                )
            ).await().txId
            withContext(Dispatchers.Main) {
                navigator?.goToSuccess(
                    bindString(R.string.success_unstake),
                    txId
                )
            }
        }.handleError {
            navigator?.showError(it.localizedMessage ?: bindString(R.string.common_error))
        }.finally {
            progressMode.content()
        }.run(jobComposite)
    }

    private fun onAnswerSelected(id: Long) {
        val poll: Poll = poll.value ?: return
        if (poll.isFinished()) return
        val selectedAnswer: Answer = AnswerDao.db.getAnswerById(poll.id, id) ?: return
        navigator?.goToVote(poll, selectedAnswer)
    }
}