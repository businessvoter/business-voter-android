package com.businessvoter.ui.polls

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.MenuItem
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.businessvoter.R
import com.businessvoter.model.ui.PollUI
import com.businessvoter.ui.base.BaseVMActivity
import com.businessvoter.ui.base.RecyclerClickListener
import com.businessvoter.ui.base.empty.EmptyNavigator
import com.businessvoter.ui.details.PollDetailsActivity
import com.businessvoter.util.extentions.bindRefreshing
import com.businessvoter.util.extentions.bindVisibility
import kotlinx.android.synthetic.main.activity_polls.*

class PollsActivity : BaseVMActivity<PollsViewModel, EmptyNavigator>() {
    companion object {
        fun getIntent(context: Context) =
            Intent(context, PollsActivity::class.java)
    }

    private val adapter: PollAdapter by lazy { PollAdapter() }

    private val clickListener: RecyclerClickListener<PollUI> =
        object : RecyclerClickListener<PollUI>() {
            override fun onClick(pos: Int, model: PollUI?) {
                super.onClick(pos, model ?: return)
                clickThrottler.offer {
                    startActivity(PollDetailsActivity.getIntent(this@PollsActivity, model.pollId))
                }
            }
        }

    override fun initializeView() {
        setContentView(R.layout.activity_polls)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setSupportActionBar(toolbar)

        pollsRecycler.apply {
            layoutManager = LinearLayoutManager(this@PollsActivity)
            this@PollsActivity.adapter.setClickListener(clickListener)
            adapter = this@PollsActivity.adapter
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean =
        when (item.itemId) {
            android.R.id.home -> onBackPressed().let { true }
            else -> super.onOptionsItemSelected(item)
        }

    override fun bindViews() {
        super.bindViews()
        viewModel.let {
            pollsRecycler.bindVisibility(this, it.isRecyclerVisible)
            pollsEmptyView.bindVisibility(this, it.isEmptyViewVisible)
            pollsSwipeRefresh.bindVisibility(this, it.isRecyclerVisible)
            pollsSwipeRefresh.bindRefreshing(this, it.isRefreshing)

            it.models.observe(this, Observer {
                adapter.applyChanges(it)
            })
        }
    }

    override fun bindClicksAndListeners() {
        super.bindClicksAndListeners()
        pollsSwipeRefresh.setOnRefreshListener { viewModel.onRefresh() }
    }

    override fun provideViewModel(): PollsViewModel =
        ViewModelProvider(this).get(PollsViewModel::class.java)

    override fun provideNavigator(): EmptyNavigator = object : EmptyNavigator {}
}