package com.businessvoter.ui.vote

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.businessvoter.BuildConfig.SMART_CONTRACT
import com.businessvoter.R
import com.businessvoter.api.Api
import com.businessvoter.model.api.EthPostTransactionRequestBody
import com.businessvoter.model.db.Answer
import com.businessvoter.model.db.Poll
import com.businessvoter.prefs.UserPrefs
import com.businessvoter.ui.base.BaseViewModel
import com.businessvoter.util.crypto.DataActionEncoder
import com.businessvoter.util.extentions.*
import com.businessvoter.util.formatAmount
import com.businessvoter.util.toBigDecimalSafe
import com.businessvoter.util.toLongSafe
import com.paytomat.eth.utils.Numeric
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import org.bouncycastle.util.encoders.Hex
import org.web3j.crypto.Credentials
import org.web3j.crypto.ECKeyPair
import org.web3j.crypto.RawTransaction
import org.web3j.crypto.TransactionEncoder
import java.math.BigDecimal
import java.math.BigInteger
import java.text.SimpleDateFormat
import java.util.*

class VoteViewModel : BaseViewModel<VoteNavigator>() {
    val progressMode: ProgressModeLiveData = ProgressModeLiveData()
    val poll: MutableLiveData<Poll> = MutableLiveData()
    val answer: MutableLiveData<Answer> = MutableLiveData()
    val amount: MutableLiveData<String> = MutableLiveData("")

    val description: LiveData<String> = poll.zip(answer).map { (poll, answer) ->
        bindString(
            R.string.new_vote_description,
            UserPrefs.tokenSymbol,
            answer.text,
            SimpleDateFormat(
                "dd.MM.yyyy 'at' HH:mm:ss",
                Locale.getDefault()
            ).format(Date(poll.endTime.secToMs()))
        )
    }

    fun onVoteClick() {
        val answerText: String = answer.value?.text ?: ""
        val userBalance: BigDecimal = UserPrefs.userTokenBalance.toBigDecimalSafe()
        val amount: BigDecimal = amount.value.toBigDecimalSafe()

        if (amount > userBalance) {
            navigator?.showError(bindString(R.string.common_error_amount_is_more_than_balance))
        } else {
            navigator?.goToConfirmPin(
                bindString(
                    R.string.new_vote_confirm_text,
                    formatAmount(amount.toString()),
                    answerText
                )
            )
        }
    }

    fun onVoteConfirmed() {
        val poll: Poll = poll.value ?: return
        val answer: Answer = answer.value ?: return
        progressMode.progress()
        JobBuilder {
            val privateKey: ByteArray = Hex.decode(UserPrefs.privateKey)
            val credentials: Credentials = Credentials.create(ECKeyPair.create(privateKey))
            val nonce: BigInteger = BigInteger.valueOf(Api.api.getNonce().await().nonce)
            val gasLimit: BigInteger = BigInteger.valueOf(800000)
            val gasPrice: BigInteger =
                BigInteger.valueOf(Api.api.getGasPrice().await().gasPrice.toLong())
            val chainId: Byte = 3

            val data = DataActionEncoder.encodeStakeData(
                SMART_CONTRACT,
                BigInteger.valueOf(amount.get().toLongSafe()),
                BigInteger.valueOf(poll.id),
                BigInteger.valueOf(answer.id)
            )

            val rawTransaction: RawTransaction =
                RawTransaction.createTransaction(
                    nonce,
                    gasPrice,
                    gasLimit,
                    SMART_CONTRACT,
                    BigInteger.ZERO,
                    data
                )
            val signedMessage: ByteArray =
                TransactionEncoder.signMessage(rawTransaction, chainId, credentials)

            val hexMessage: String = Numeric.toHexString(signedMessage)
            val txId: String = Api.api.postTransaction(
                EthPostTransactionRequestBody(
                    hexMessage.substring(2)
                )
            ).await().txId
            withContext(Dispatchers.Main) {
                navigator?.goToSuccess(
                    bindString(R.string.success_stake, formatAmount(amount.get()), answer.text),
                    txId
                )
            }
        }.handleError {
            navigator?.showError(it.localizedMessage ?: bindString(R.string.common_error))
        }.finally {
            progressMode.content()
        }.run(jobComposite)
    }
}