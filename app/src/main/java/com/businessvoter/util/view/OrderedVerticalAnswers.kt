package com.businessvoter.util.view

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import android.widget.LinearLayout
import android.widget.TextView
import androidx.annotation.LayoutRes
import androidx.constraintlayout.widget.ConstraintLayout
import com.businessvoter.R
import com.businessvoter.model.db.Answer
import com.businessvoter.util.extentions.bindColor
import com.businessvoter.util.extentions.bindString
import com.businessvoter.util.toBigDecimalSafe
import java.math.BigDecimal

class OrderedVerticalAnswers @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyle: Int = 0
) : LinearLayout(context, attrs, defStyle) {

    init {
        this.apply {
            orientation = VERTICAL
            layoutParams = LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT)
        }
    }

    fun setData(data: List<OrderedVerticalAnswerItem>) {
        this.removeAllViews()
        data.forEachIndexed { index, item ->
            @LayoutRes val layoutRes: Int = R.layout.item_answer

            val itemView = LayoutInflater.from(context).inflate(layoutRes, this, false)
            this.addView(itemView)

            val title: TextView = itemView.findViewById(R.id.itemAnswerTxtTitle)
            val checkedImage: View = itemView.findViewById(R.id.itemAnswerImgCheck)
            val number: TextView = itemView.findViewById(R.id.itemAnswerTxtNumber)
            val percent: TextView = itemView.findViewById(R.id.itemAnswerTxtResult)
            val background: ConstraintLayout = itemView.findViewById(R.id.itemAnswerLayoutBg)

            title.text = item.text
            checkedImage.visibility = if (item.checked) View.VISIBLE else View.GONE
            number.visibility = if (!item.checked) View.VISIBLE else View.GONE
            number.text = (index + 1).toString()
            itemView.setOnClickListener {
                item.onClick(item.id)
            }

            if (item.hasFinished) {
                percent.text = bindString(R.string.poll_details_percent_result, item.votedPercent)
                if (item.isWinner) {
                    background.setBackgroundColor(bindColor(R.color.colorPrimary30))
                }
            }
        }
    }
}

data class OrderedVerticalAnswerItem(
    val id: Long,
    val checked: Boolean,
    val text: String,
    val onClick: (Long) -> Unit,
    val votedPercent: Int = 0,
    val hasFinished: Boolean = false,
    val isWinner: Boolean = false
) {
    constructor(
        answer: Answer,
        onClick: (Long) -> Unit,
        votedPercent: Int = 0,
        hasFinished: Boolean = false,
        isWinner: Boolean = false
    ) : this(
        answer.id,
        answer.userStaked.toBigDecimalSafe() > BigDecimal.ZERO,
        answer.text,
        onClick,
        votedPercent,
        hasFinished,
        isWinner
    )
}