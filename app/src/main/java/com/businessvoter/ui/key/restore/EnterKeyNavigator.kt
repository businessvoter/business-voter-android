package com.businessvoter.ui.key.restore

import com.businessvoter.model.UserInfo
import com.businessvoter.ui.base.BaseNavigator

interface EnterKeyNavigator : BaseNavigator {
    fun showError(errorText: String)
    fun goToTokenAddressInput(userInfo: UserInfo)
}