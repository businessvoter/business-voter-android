package com.businessvoter.ui.key.restore

import android.app.AlertDialog
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.MenuItem
import androidx.lifecycle.ViewModelProvider
import com.businessvoter.R
import com.businessvoter.model.UserInfo
import com.businessvoter.ui.address.EnterTokenContractActivity
import com.businessvoter.ui.base.BaseVMActivity
import com.businessvoter.util.extentions.bindTextChange
import com.businessvoter.util.extentions.setOnClickThrottled
import kotlinx.android.synthetic.main.activity_enter_key.*

class EnterKeyActivity : BaseVMActivity<EnterKeyViewModel, EnterKeyNavigator>() {

    companion object {
        fun getIntent(context: Context) = Intent(context, EnterKeyActivity::class.java)
    }

    override fun initializeView() {
        setContentView(R.layout.activity_enter_key)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setSupportActionBar(toolbar)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean =
        when (item.itemId) {
            android.R.id.home -> onBackPressed().let { true }
            else -> super.onOptionsItemSelected(item)
        }

    override fun bindViews() {
        super.bindViews()
        enterKeyEditText.bindTextChange(this, viewModel.privateKey)
    }

    override fun bindClicksAndListeners() {
        super.bindClicksAndListeners()
        enterKeyBtnContinue.setOnClickThrottled {
            viewModel.onContinueClick()
        }
    }

    override fun provideViewModel(): EnterKeyViewModel =
        ViewModelProvider(this).get(EnterKeyViewModel::class.java)

    override fun provideNavigator(): EnterKeyNavigator = object :
        EnterKeyNavigator {
        override fun showError(errorText: String) {
            AlertDialog.Builder(this@EnterKeyActivity)
                .setTitle(R.string.common_error)
                .setMessage(errorText)
                .setPositiveButton(R.string.common_ok, null)
                .show()
        }

        override fun goToTokenAddressInput(userInfo: UserInfo) {
            startActivity(EnterTokenContractActivity.getIntent(this@EnterKeyActivity, userInfo))
        }
    }
}