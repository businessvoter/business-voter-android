package com.businessvoter.ui.base

import androidx.annotation.CallSuper
import androidx.lifecycle.ViewModel
import com.businessvoter.util.ClickThrottler
import com.businessvoter.util.JobComposite
import java.lang.ref.WeakReference

open class BaseViewModel<N : BaseNavigator> : ViewModel() {

    private var navigatorRef: WeakReference<N?> = WeakReference(null)
    val jobComposite = JobComposite()

    val clickThrottler = ClickThrottler()

    var navigator
        get() = navigatorRef.get()
        set(value) {
            navigatorRef = WeakReference(value)
            onUiAttached()
        }

    open fun onUiAttached() {

    }

    @CallSuper
    override fun onCleared() {
        super.onCleared()
        jobComposite.cancel()
    }

}