package com.businessvoter.util

class ClickThrottler(val delay: Long = DEFAULT_DELAY) {

    companion object {

        const val DEFAULT_DELAY = 350L
    }

    private var lastOffer = 0L

    fun offer(action: () -> Unit) {
        val now = System.currentTimeMillis()
        if (now - lastOffer > delay) {
            lastOffer = now
            action()
        }
    }
}