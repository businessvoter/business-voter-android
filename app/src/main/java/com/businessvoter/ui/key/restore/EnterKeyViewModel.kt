package com.businessvoter.ui.key.restore

import androidx.lifecycle.MutableLiveData
import com.businessvoter.R
import com.businessvoter.model.UserInfo
import com.businessvoter.ui.base.BaseViewModel
import com.businessvoter.util.EthHelper.getInfoFromPrivateKey
import com.businessvoter.util.extentions.bindString
import com.businessvoter.util.extentions.get

class EnterKeyViewModel : BaseViewModel<EnterKeyNavigator>() {
    val privateKey: MutableLiveData<String> = MutableLiveData("")

    fun onContinueClick() {
        try {
            val validInfo: UserInfo = getInfoFromPrivateKey(privateKey.get())
            navigator?.goToTokenAddressInput(validInfo)
        } catch (e: Exception) {
            navigator?.showError(bindString(R.string.common_error_invalid_pk))
        }
    }
}