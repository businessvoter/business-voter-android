package com.businessvoter.model.api

data class UserLoginPostRequest(
    val address: String,
    val signature: String,
    val tokenContract: String
)

data class UserLoginPostResponse(
    val token: String,
    val user: UserResponse
)

data class UserResponse(
    val address: String,
    val balance: String,
    val polls: Array<PollResponse>,
    val staked: String,
    val token: TokenResponse
) {
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as UserResponse

        if (address != other.address) return false
        if (balance != other.balance) return false
        if (!polls.contentEquals(other.polls)) return false
        if (staked != other.staked) return false
        if (token != other.token) return false

        return true
    }

    override fun hashCode(): Int {
        var result = address.hashCode()
        result = 31 * result + balance.hashCode()
        result = 31 * result + polls.contentHashCode()
        result = 31 * result + staked.hashCode()
        result = 31 * result + token.hashCode()
        return result
    }
}

data class PollResponse(
    val answers: Array<AnswerResponse>,
    val created: Long,
    val endTime: Long,
    val id: Long,
    val interest: InterestResponse,
    val text: String,
    val totalStaked: String,
    val unstaked: Boolean,
    val userStaked: String
) {
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as PollResponse

        if (!answers.contentEquals(other.answers)) return false
        if (created != other.created) return false
        if (endTime != other.endTime) return false
        if (id != other.id) return false
        if (interest != other.interest) return false
        if (text != other.text) return false
        if (totalStaked != other.totalStaked) return false
        if (unstaked != other.unstaked) return false
        if (userStaked != other.userStaked) return false

        return true
    }

    override fun hashCode(): Int {
        var result = answers.contentHashCode()
        result = 31 * result + created.hashCode()
        result = 31 * result + endTime.hashCode()
        result = 31 * result + id.hashCode()
        result = 31 * result + interest.hashCode()
        result = 31 * result + text.hashCode()
        result = 31 * result + totalStaked.hashCode()
        result = 31 * result + unstaked.hashCode()
        result = 31 * result + userStaked.hashCode()
        return result
    }
}

data class InterestResponse(
    val denominator: String,
    val numerator: String
)

data class AnswerResponse(
    val id: Long,
    val text: String,
    val totalStaked: String,
    val userStaked: String
)

data class TokenResponse(
    val balance: String,
    val contractAddress: String,
    val decimals: Int,
    val name: String,
    val symbol: String
)