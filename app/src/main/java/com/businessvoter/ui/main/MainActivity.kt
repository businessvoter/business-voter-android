package com.businessvoter.ui.main

import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.businessvoter.R
import com.businessvoter.model.ui.PollUI
import com.businessvoter.ui.base.BaseVMActivity
import com.businessvoter.ui.base.RecyclerClickListener
import com.businessvoter.ui.base.empty.EmptyNavigator
import com.businessvoter.ui.details.PollDetailsActivity
import com.businessvoter.ui.polls.PollsActivity
import com.businessvoter.ui.send.SendActivity
import com.businessvoter.util.extentions.bindRefreshing
import com.businessvoter.util.extentions.bindText
import com.businessvoter.util.extentions.bindVisibility
import com.businessvoter.util.extentions.setOnClickThrottled
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : BaseVMActivity<MainViewModel, EmptyNavigator>() {
    companion object {
        fun getIntent(context: Context) =
            Intent(context, MainActivity::class.java)
                .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
                .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
    }

    private val adapter: MainPollAdapter by lazy { MainPollAdapter() }

    private val clickListener: RecyclerClickListener<PollUI> =
        object : RecyclerClickListener<PollUI>() {
            override fun onClick(pos: Int, model: PollUI?) {
                super.onClick(pos, model ?: return)
                clickThrottler.offer {
                    startActivity(PollDetailsActivity.getIntent(this@MainActivity, model.pollId))
                }
            }
        }

    override fun initializeView() {
        setContentView(R.layout.activity_main)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mainRecycler.apply {
            layoutManager = LinearLayoutManager(this@MainActivity)
            this@MainActivity.adapter.setClickListener(clickListener)
            adapter = this@MainActivity.adapter
        }
    }

    override fun bindViews() {
        super.bindViews()
        viewModel.let {
            mainTxtBalance.bindText(this, it.balance)
            mainTxtStaked.bindText(this, it.staked)
            mainRecycler.bindVisibility(this, it.isRecyclerVisible)
            mainEmptyView.bindVisibility(this, it.isEmptyViewVisible)
            mainSwipeRefresh.bindVisibility(this, it.isRecyclerVisible)
            mainSwipeRefresh.bindRefreshing(this, it.isRefreshing)

            it.models.observe(this, Observer {
                adapter.applyChanges(it)
            })
        }
    }

    override fun bindClicksAndListeners() {
        super.bindClicksAndListeners()
        mainSwipeRefresh.setOnRefreshListener { viewModel.onRefresh() }
        mainBtnSend.setOnClickThrottled {
            startActivity(SendActivity.getIntent(this))
        }
        mainBtnVote.setOnClickThrottled {
            startActivity(PollsActivity.getIntent(this))
        }
    }

    override fun provideViewModel(): MainViewModel =
        ViewModelProvider(this).get(MainViewModel::class.java)

    override fun provideNavigator(): EmptyNavigator = object : EmptyNavigator {}
}