package com.businessvoter.ui.base.empty

import com.businessvoter.ui.base.BaseNavigator
import com.businessvoter.ui.base.BaseViewModel

class EmptyViewModel<N : BaseNavigator> : BaseViewModel<N>()