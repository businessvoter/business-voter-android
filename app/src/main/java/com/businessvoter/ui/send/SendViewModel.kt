package com.businessvoter.ui.send

import androidx.lifecycle.MutableLiveData
import com.businessvoter.BuildConfig
import com.businessvoter.R
import com.businessvoter.api.Api
import com.businessvoter.model.api.EthPostTransactionRequestBody
import com.businessvoter.prefs.UserPrefs
import com.businessvoter.ui.base.BaseViewModel
import com.businessvoter.util.crypto.DataActionEncoder
import com.businessvoter.util.extentions.JobBuilder
import com.businessvoter.util.extentions.ProgressModeLiveData
import com.businessvoter.util.extentions.bindString
import com.businessvoter.util.extentions.get
import com.businessvoter.util.formatAmount
import com.businessvoter.util.toBigDecimalSafe
import com.paytomat.eth.utils.Numeric
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import org.bouncycastle.util.encoders.Hex
import org.web3j.crypto.Credentials
import org.web3j.crypto.ECKeyPair
import org.web3j.crypto.RawTransaction
import org.web3j.crypto.TransactionEncoder
import java.math.BigDecimal
import java.math.BigInteger

class SendViewModel : BaseViewModel<SendNavigator>() {
    val progressMode: ProgressModeLiveData = ProgressModeLiveData()
    val to: MutableLiveData<String> = MutableLiveData("")
    val amount: MutableLiveData<String> = MutableLiveData("")

    fun onSendClick() {
        val userBalance: BigDecimal = UserPrefs.userTokenBalance.toBigDecimalSafe()
        val amount: BigDecimal = amount.value.toBigDecimalSafe()

        if (amount > userBalance) {
            navigator?.showError(bindString(R.string.common_error_amount_is_more_than_balance))
        } else {
            navigator?.goToConfirmPin(
                bindString(
                    R.string.send_confirm_text,
                    formatAmount(amount.toString()),
                    to.get()
                )
            )
        }
    }

    fun onSendConfirmed() {
        progressMode.progress()
        JobBuilder {
            val privateKey: ByteArray = Hex.decode(UserPrefs.privateKey)
            val credentials: Credentials = Credentials.create(ECKeyPair.create(privateKey))
            val nonce: BigInteger = BigInteger.valueOf(Api.api.getNonce().await().nonce)
            val gasLimit: BigInteger = BigInteger.valueOf(800000)
            val gasPrice: BigInteger =
                BigInteger.valueOf(Api.api.getGasPrice().await().gasPrice.toLong())
            val chainId: Byte = 3
            
            val data = DataActionEncoder.encodeTransferData(
                to.get(),
                (amount.get()
                    .toBigDecimalSafe() * BigDecimal.TEN.pow(UserPrefs.tokenDecimals)).toBigInteger()
            )

            val rawTransaction: RawTransaction =
                RawTransaction.createTransaction(
                    nonce,
                    gasPrice,
                    gasLimit,
                    BuildConfig.SMART_CONTRACT,
                    BigInteger.ZERO,
                    data
                )
            val signedMessage: ByteArray =
                TransactionEncoder.signMessage(rawTransaction, chainId, credentials)

            val hexMessage: String = Numeric.toHexString(signedMessage)
            val txId: String = Api.api.postTransaction(
                EthPostTransactionRequestBody(
                    hexMessage.substring(2)
                )
            ).await().txId
            withContext(Dispatchers.Main) {
                navigator?.goToSuccess(
                    bindString(R.string.success_send, formatAmount(amount.get()), to.get()),
                    txId
                )
            }
        }.handleError {
            navigator?.showError(it.localizedMessage ?: bindString(R.string.common_error))
        }.finally {
            progressMode.content()
        }.run(jobComposite)
    }
}