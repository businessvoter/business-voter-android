package com.businessvoter.ui.details

import com.businessvoter.model.db.Answer
import com.businessvoter.model.db.Poll
import com.businessvoter.ui.base.BaseNavigator

interface PollDetailsNavigator : BaseNavigator {
    fun goToVote(poll: Poll, answer: Answer)
    fun showError(errorText: String)
    fun goToConfirmPin(confirmText: String)
    fun goToSuccess(successText: String, txId: String)
}