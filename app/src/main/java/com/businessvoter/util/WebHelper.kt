package com.businessvoter.util

import android.content.Context
import android.content.Intent
import android.net.Uri
import androidx.browser.customtabs.CustomTabsIntent

object WebHelper {
    private const val SERVICE_ACTION = "android.support.customtabs.action.CustomTabsService"
    private const val CHROME_PACKAGE = "com.android.chrome"

    private fun isChromeCustomTabsSupported(context: Context): Boolean {
        val serviceIntent = Intent(SERVICE_ACTION)
        serviceIntent.`package` = CHROME_PACKAGE
        val resolveInfo = context.packageManager.queryIntentServices(serviceIntent, 0)
        return resolveInfo.isNotEmpty()
    }

    fun runWebPage(context: Context, url: String) {
        val uri: Uri = Uri.parse(url)
        try {
            if (isChromeCustomTabsSupported(context))
                CustomTabsIntent.Builder()
                    .build()
                    .launchUrl(context, uri)
            else context.startActivity(Intent(Intent.ACTION_VIEW, Uri.parse(url)))
        } catch (e: Exception) {

        }
    }

}