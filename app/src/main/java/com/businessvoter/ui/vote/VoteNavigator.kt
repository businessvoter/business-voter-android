package com.businessvoter.ui.vote

import com.businessvoter.ui.base.BaseNavigator

interface VoteNavigator : BaseNavigator {
    fun showError(errorText: String)
    fun goToConfirmPin(confirmText: String)
    fun goToSuccess(successText: String, txId: String)
}