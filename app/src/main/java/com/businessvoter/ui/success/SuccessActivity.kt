package com.businessvoter.ui.success

import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.lifecycle.ViewModelProvider
import com.businessvoter.R
import com.businessvoter.ui.base.BaseVMActivity
import com.businessvoter.util.WebHelper
import com.businessvoter.util.extentions.bindText
import com.businessvoter.util.extentions.set
import com.businessvoter.util.extentions.setOnClickThrottled
import kotlinx.android.synthetic.main.activity_success.*

class SuccessActivity : BaseVMActivity<SuccessViewModel, SuccessNavigator>() {

    companion object {
        private val KEY_SUCCESS_TEXT = "KEY_SUCCESS_TEXT"
        private val KEY_TX_ID = "KEY_TX_ID"

        fun getIntent(context: Context, successText: String, txId: String) =
            Intent(context, SuccessActivity::class.java)
                .putExtra(KEY_SUCCESS_TEXT, successText)
                .putExtra(KEY_TX_ID, txId)
    }

    override fun initializeView() {
        setContentView(R.layout.activity_success)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        viewModel.successText.set(intent.getStringExtra(KEY_SUCCESS_TEXT) ?: return)
        viewModel.txId.set(intent.getStringExtra(KEY_TX_ID) ?: return)
    }

    override fun bindViews() {
        super.bindViews()
        viewModel.let {
            successTxtSubtitle.bindText(this, it.successText)
        }
    }

    override fun bindClicksAndListeners() {
        super.bindClicksAndListeners()
        successBtnClose.setOnClickThrottled {
            finish()
        }
        successBtnConfirm.setOnClickThrottled {
            viewModel.onDetailsClick()
        }
    }

    override fun provideViewModel(): SuccessViewModel =
        ViewModelProvider(this).get(SuccessViewModel::class.java)

    override fun provideNavigator(): SuccessNavigator = object : SuccessNavigator {
        override fun navigateToWeb(url: String) {
            WebHelper.runWebPage(this@SuccessActivity, url)
        }
    }
}