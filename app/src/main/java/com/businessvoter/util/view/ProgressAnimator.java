package com.businessvoter.util.view;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ValueAnimator;
import android.os.Handler;
import android.os.Looper;
import android.view.View;
import android.view.animation.DecelerateInterpolator;

import androidx.annotation.IntRange;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.lang.ref.WeakReference;

public class ProgressAnimator {
    @Nullable
    private StoppableValueAnimator stoppableValueAnimator;
    @NonNull
    private Handler handler = new Handler(Looper.getMainLooper());
    @NonNull
    private WeakReference<View> mainContainer, inverseContainer;

    private int duration;
    private long prevAnimTime;
    private long animStartTimestamp;
    private boolean waitUntilPrevAnimFinished;

    public ProgressAnimator(@NonNull final View mainContainer,
                            @NonNull final View progressContainer,
                            @IntRange(from = 0) final int duration) {
        this(mainContainer, progressContainer, duration, true);
    }

    public ProgressAnimator(@IntRange(from = 0) final int duration) {
        this(null, null, duration, true);
    }

    public ProgressAnimator(@Nullable final View mainContainer,
                            @Nullable final View progressContainer,
                            @IntRange(from = 0) final int duration,
                            boolean waitUntilPrevAnimFinished) {
        this.mainContainer = new WeakReference<>(mainContainer);
        this.inverseContainer = new WeakReference<>(progressContainer);
        this.duration = duration;
        this.waitUntilPrevAnimFinished = waitUntilPrevAnimFinished;
        this.prevAnimTime = duration;
    }

    public void setAnimatedViews(@Nullable final View mainContainer,
                                 @Nullable final View progressContainer) {
        this.mainContainer = new WeakReference<>(mainContainer);
        this.inverseContainer = new WeakReference<>(progressContainer);
    }

    public void showProgress(boolean animate) {
        fadeOutMainView(animate);
    }

    public void hideProgress(boolean animate) {
        fadeInMainView(animate);
    }

    public void fadeInMainView(boolean animate) {
        if (animate)
            fadeInMainView();
        else
            showMainView();
    }

    public void fadeOutMainView(boolean animate) {
        if (animate)
            fadeOutMainView();
        else
            hideMainView();
    }

    public void fadeInMainView() {
        animate(1.f);
    }

    public void fadeOutMainView() {
        animate(0.f);
    }

    public void showMainView() {
        setViewShown(mainContainer());
        setViewHidden(inverseContainer());
    }

    public void hideMainView() {
        setViewShown(inverseContainer());
        setViewHidden(mainContainer());
    }

    private void animate(float finalAlpha) {
        if (mainContainer() == null || inverseContainer() == null)
            return;
        if (!waitUntilPrevAnimFinished && stoppableValueAnimator != null && stoppableValueAnimator.isRunning())
            stoppableValueAnimator.stop();

        final float startAlpha = getStartAlpha();
        final boolean isFadingIn = finalAlpha == 1.f;
        final int animTime;

        stoppableValueAnimator = new StoppableValueAnimator(ValueAnimator.ofFloat(startAlpha, finalAlpha));
        stoppableValueAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                float t = (float) animation.getAnimatedValue();
                View mainContainer = mainContainer();
                View progressContainer = inverseContainer();
                if (progressContainer != null)
                    progressContainer.setAlpha(1.f - t);
            }
        });
        stoppableValueAnimator.setInterpolator(new DecelerateInterpolator());
        if (isFadingIn) {
            stoppableValueAnimator.addListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    View mainContainer = mainContainer();
                    View inverseContainer = inverseContainer();
                    if (mainContainer != null && inverseContainer != null) {
                        mainContainer.setFocusable(true);
                        mainContainer.setClickable(true);
                        inverseContainer.setFocusable(false);
                        inverseContainer.setClickable(false);
                    }
                }
            });
            animTime = waitUntilPrevAnimFinished ? duration : (int) (duration * (1.f - startAlpha));
        } else { //fading out
            stoppableValueAnimator.addListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationStart(Animator animation) {
                    View mainContainer = mainContainer();
                    View inverseContainer = inverseContainer();
                    if (mainContainer != null && inverseContainer != null) {
                        mainContainer.setFocusable(false);
                        mainContainer.setClickable(false);
                        inverseContainer.setFocusable(true);
                        inverseContainer.setClickable(true);
                    }
                }
            });
            animTime = waitUntilPrevAnimFinished ? duration : (int) (duration * startAlpha);
        }
        stoppableValueAnimator.setDuration(animTime);

        if (waitUntilPrevAnimFinished) {
            long dt = System.currentTimeMillis() - animStartTimestamp;
            if (dt >= prevAnimTime)
                start(animTime);
            else
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        start(animTime);
                    }
                }, dt);
        } else
            start(animTime);
    }

    private float getStartAlpha() {
        View v = mainContainer();
        return v == null ? 0 : v.getAlpha();
    }

    private void start(int animTime) {
        if (stoppableValueAnimator == null)
            return;
        animStartTimestamp = System.currentTimeMillis();
        prevAnimTime = animTime;
        stoppableValueAnimator.start();
    }

    private void setViewShown(@Nullable View view) {
        if (view == null)
            return;
        view.setAlpha(1.f);
        view.setClickable(true);
        view.setFocusable(true);
    }

    private void setViewHidden(@Nullable View view) {
        if (view == null)
            return;
        view.setAlpha(0.f);
        view.setClickable(false);
        view.setFocusable(false);
    }

    @Nullable
    private View mainContainer() {
        return mainContainer.get();
    }

    @Nullable
    private View inverseContainer() {
        return inverseContainer.get();
    }

    public void cleanup() {
        handler.removeCallbacksAndMessages(null);
        if (stoppableValueAnimator != null)
            stoppableValueAnimator.stop();
        mainContainer.clear();
        inverseContainer.clear();
    }
}