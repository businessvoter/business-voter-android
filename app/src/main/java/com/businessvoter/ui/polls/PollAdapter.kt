package com.businessvoter.ui.polls

import android.view.LayoutInflater
import android.view.ViewGroup
import com.businessvoter.R
import com.businessvoter.model.ui.PollUI
import com.businessvoter.ui.base.BaseRecyclerAdapter
import com.businessvoter.ui.base.BaseViewHolder

class PollAdapter : BaseRecyclerAdapter<PollUI>() {
    override fun provideViewHolder(
        layoutInflater: LayoutInflater,
        parent: ViewGroup,
        viewType: Int
    ): BaseViewHolder<PollUI> =
        PollViewHolder(layoutInflater.inflate(R.layout.item_poll, parent, false))
}