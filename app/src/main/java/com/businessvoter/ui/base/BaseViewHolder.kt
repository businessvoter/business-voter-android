package com.businessvoter.ui.base

import android.view.View
import androidx.recyclerview.widget.RecyclerView

abstract class BaseViewHolder<M>(itemView: View) : RecyclerView.ViewHolder(itemView) {

    protected var model: M? = null
    protected var clickListener: RecyclerClickListener<M>? = null

    fun bind(model: M, clickListener: RecyclerClickListener<M>?) {
        this.clickListener = clickListener
        this.model = model
        bind(model)
    }

    abstract fun bind(model: M)

    open fun clearUp() {
        clickListener = null
    }
}