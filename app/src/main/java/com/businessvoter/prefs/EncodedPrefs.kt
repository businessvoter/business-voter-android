package com.businessvoter.prefs

import android.content.Context
import android.content.SharedPreferences
import com.businessvoter.BuildConfig

import com.businessvoter.BusinessVoterApp
import com.businessvoter.util.StringArray
import com.businessvoter.util.extentions.readString
import com.businessvoter.util.fromBase64
import com.businessvoter.util.toBase64
import com.google.gson.Gson
import java.security.Key
import java.util.*
import javax.crypto.Cipher
import javax.crypto.spec.SecretKeySpec

class EncodedPrefs(val name: String) {

    private val prefs: SharedPreferences by lazy {
        BusinessVoterApp.app.getSharedPreferences(
            name,
            Context.MODE_PRIVATE
        )
    }
    private val secret: Key = SecretKeySpec(BuildConfig.PREF_ENCODE_KEY.toByteArray(), "AES")

    fun getAll(): Map<String, String> {
        val all: Map<String, *> = prefs.all
        val decryptedKeys: List<String> = all.keys
            .map { fromBase64(it) }
            .map { provideCipher(Cipher.DECRYPT_MODE).doFinal(it) }
            .map { String(it) }
        return decryptedKeys.map { it to readString(it, "") }.toMap()
    }

    fun hasKey(key: String): Boolean = prefs.contains(toEncodedKey(key))

    fun toEncodedKey(key: String): String =
        toBase64(provideCipher(Cipher.ENCRYPT_MODE).doFinal(key.toByteArray()))

    fun writeString(key: String, value: String) {
        if (BuildConfig.ENCODE_PREFS) {
            val encryptedKey =
                toBase64(provideCipher(Cipher.ENCRYPT_MODE).doFinal(key.toByteArray()))
            val encryptedValue =
                toBase64(provideCipher(Cipher.ENCRYPT_MODE).doFinal(value.toByteArray()))
            prefs.edit().putString(encryptedKey, encryptedValue).commit()
        } else {
            prefs.edit().putString(key, value).commit()
        }
    }

    fun readString(key: String, defaultVal: String = ""): String = if (BuildConfig.ENCODE_PREFS) {
        val encryptedKey = toBase64(provideCipher(Cipher.ENCRYPT_MODE).doFinal(key.toByteArray()))
        val encryptedValue = prefs.readString(encryptedKey, "")
        if (encryptedValue.isBlank()) defaultVal else String(
            provideCipher(Cipher.DECRYPT_MODE).doFinal(
                fromBase64(encryptedValue)
            )
        )
    } else {
        prefs.readString(key, defaultVal)
    }

    fun provideCipher(mode: Int): Cipher =
        Cipher.getInstance("AES").apply { init(mode, secret) }

    fun writeBoolean(key: String, value: Boolean) {
        writeString(key, value.toString())
    }

    fun readBoolean(key: String, defaultVal: Boolean = true): Boolean {
        val strVal = readString(key)
        if (strVal.isBlank()) return defaultVal
        return strVal.toBoolean()
    }

    fun writeStringArray(key: String, value: StringArray) {
        writeString(key, Arrays.toString(value))
    }

    fun readStringArray(key: String, defaultVal: StringArray = emptyArray()): StringArray {
        val strVal = readString(key)
        if (strVal.isBlank()) return defaultVal
        return strVal.replace("[", "", true).replace("]", "", true).split(", ").toTypedArray()
    }

    inline fun <reified T> writeObjectArray(key: String, value: Array<T>) {
        val gson = Gson()
        val stringArray: StringArray = value.map { gson.toJson(it) }.toTypedArray()
        writeStringArray(key, stringArray)
    }

    inline fun <reified T> readObjectArray(
        key: String,
        defaultVal: Array<T> = arrayOf()
    ): Array<T> {
        val stringArray = readStringArray(key, emptyArray())
        if (stringArray.isEmpty()) return defaultVal
        val gson = Gson()
        return stringArray.map { gson.fromJson(it, T::class.java) }.toTypedArray()
    }

    fun writeInt(key: String, value: Int) {
        writeString(key, value.toString())
    }

    fun readInt(key: String, defaultVal: Int = 0): Int {
        val valueStr = readString(key, "")
        if (valueStr.isBlank()) return defaultVal
        return valueStr.toIntOrNull() ?: defaultVal
    }

    fun writeLong(key: String, value: Long) {
        writeString(key, value.toString())
    }

    fun readLong(key: String, defaultVal: Long = 0): Long {
        val valueStr = readString(key, "")
        if (valueStr.isBlank()) return defaultVal
        return valueStr.toLongOrNull() ?: defaultVal
    }

    fun clear() {
        prefs.edit().clear().commit()
    }

    fun removeKey(key: String) {
        val encryptedKey = toBase64(provideCipher(Cipher.ENCRYPT_MODE).doFinal(key.toByteArray()))
        prefs.edit().remove(encryptedKey).apply()
    }

    fun getPref(): SharedPreferences = prefs

}