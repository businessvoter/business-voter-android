package com.businessvoter.api

import com.businessvoter.BuildConfig.API_URL

object Api {
    val api = ApiFactory.provideAPIService(BusinessVoterService::class, API_URL)
}