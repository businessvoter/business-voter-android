package com.businessvoter.util.extentions

import android.util.Log
import com.businessvoter.util.JobComposite
import kotlinx.coroutines.*

fun Job.cancelWith(composite: JobComposite): Job {
    composite.add(this)
    return this
}

class JobBuilder(private val task: suspend CoroutineScope.() -> Unit) {
    private var context: CoroutineDispatcher = Dispatchers.Default
    private var errorHandler: CoroutineExceptionHandler = CoroutineExceptionHandler { _, e ->
        GlobalScope.launch(
            Dispatchers.Main
        ) { throw e }
    }
    private var cancelComposite: JobComposite = JobComposite()
    private var lastCallback: () -> Unit = {}

    fun withContext(context: CoroutineDispatcher): JobBuilder {
        this.context = context
        return this
    }

    fun handleError(errHandler: (Throwable) -> Unit): JobBuilder {
        this.errorHandler = CoroutineExceptionHandler { _, e ->
            GlobalScope.launch(Dispatchers.Main) {
                errHandler(e)
                guard { lastCallback() }
            }
        }
        return this
    }

    fun guard(composite: JobComposite? = null) {
        this.errorHandler = CoroutineExceptionHandler { _, _ -> }
        if (composite != null) run(composite) else run()
    }

    fun cancelWith(composite: JobComposite): JobBuilder {
        this.cancelComposite = composite
        return this
    }

    fun run(): Job = GlobalScope.launch(context + errorHandler) {
        task()
        guard { lastCallback() }
    }.cancelWith(cancelComposite)

    fun run(composite: JobComposite): Job = cancelWith(composite).run()

    fun runSafe(): Job = handleError {
        Log.w("err", "error occurred", it)
    }.run()

    fun runSafe(composite: JobComposite): Job = handleError {
        Log.w("err", "error occurred", it)
    }.run(composite)

    fun finally(callback: () -> Unit): JobBuilder = this.apply {
        lastCallback = callback
    }
}

inline fun <T> guard(func: () -> T): T? = try {
    func()
} catch (e: Exception) {
    null
}
