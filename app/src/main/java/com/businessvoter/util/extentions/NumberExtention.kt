package com.businessvoter.util.extentions

import java.math.RoundingMode
import java.text.DecimalFormat
import java.text.DecimalFormatSymbols
import java.util.*

fun Number.format(formatPattern: String): String =
    DecimalFormat(formatPattern, DecimalFormatSymbols.getInstance(Locale.ENGLISH)).apply {
        roundingMode = RoundingMode.FLOOR
    }.format(this)