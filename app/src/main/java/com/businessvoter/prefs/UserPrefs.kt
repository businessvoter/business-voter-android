package com.businessvoter.prefs

import android.os.Build
import android.util.Log
import androidx.lifecycle.LiveData
import com.businessvoter.model.api.TokenResponse
import com.businessvoter.model.api.UserResponse
import com.businessvoter.util.JobComposite
import com.businessvoter.util.StringEncodedPrefsLiveData
import com.businessvoter.util.crypto.BCrypt
import com.businessvoter.util.crypto.BCrypt.checkpw
import com.businessvoter.util.extentions.JobBuilder
import com.businessvoter.util.fromBase64
import com.businessvoter.util.toBase64
import kotlinx.coroutines.*
import kotlinx.coroutines.Dispatchers.Default
import java.security.SecureRandom
import javax.crypto.Cipher
import javax.crypto.spec.SecretKeySpec

object UserPrefs {
    private const val KEY_ACCESS_TOKEN = "KEY_ACCESS_TOKEN"
    private const val KEY_SALT = "KEY_SALT"
    private const val KEY_PIN = "KEY_PIN"
    private const val KEY_PRIVATE_KEY = "KEY_PRIVATE_KEY"

    private const val KEY_USER_ADDRESS = "KEY_USER_ADDRESS"
    private const val KEY_USER_ETH_BALANCE = "KEY_USER_ETH_BALANCE"
    private const val KEY_USER_TOKEN_BALANCE = "KEY_USER_TOKEN_BALANCE"
    private const val KEY_USER_STAKED = "KEY_USER_STAKED"

    private const val KEY_TOKEN_NAME = "KEY_TOKEN_NAME"
    private const val KEY_TOKEN_SYMBOL = "KEY_TOKEN_SYMBOL"
    private const val KEY_TOKEN_CONTRACT_ADDRESS = "KEY_TOKEN_CONTRACT_ADDRESS"
    private const val KEY_TOKEN_DECIMALS = "KEY_TOKEN_DECIMALS"

    private val jobComposite = JobComposite()

    private val prefs: EncodedPrefs by lazy { EncodedPrefs("userPrefs") }

    var accessToken: String
        set(value) = prefs.writeString(KEY_ACCESS_TOKEN, value)
        get() = prefs.readString(KEY_ACCESS_TOKEN, "")

    val salt: String
        get() {
            var salt = prefs.readString(KEY_SALT)
            if (salt.isBlank()) {
                val saltBytes = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                    SecureRandom.getInstanceStrong()
                } else {
                    SecureRandom()
                }.generateSeed(24)
                salt = toBase64(saltBytes)
                prefs.writeString(KEY_SALT, salt)
            }
            return salt
        }

    private var pinHash: String
        set(hasedPin) {
            val cipher = Cipher.getInstance("AES")
            cipher.init(Cipher.ENCRYPT_MODE, SecretKeySpec(fromBase64(salt), "AES"))
            val encodedPinBytes = cipher.doFinal(hasedPin.toByteArray())
            prefs.writeString(KEY_PIN, toBase64(encodedPinBytes))
        }
        get() {
            val encodedPinBase64 = prefs.readString(KEY_PIN)
            if (encodedPinBase64.isBlank()) return ""
            val encodedPinBytes = fromBase64(encodedPinBase64)
            val cipher = Cipher.getInstance("AES")
            cipher.init(Cipher.DECRYPT_MODE, SecretKeySpec(fromBase64(salt), "AES"))
            return String(cipher.doFinal(encodedPinBytes))
        }

    var privateKey: String
        set(value) = prefs.writeString(KEY_PRIVATE_KEY, value)
        get() = prefs.readString(KEY_PRIVATE_KEY, "")

    var userAddress: String
        set(value) = prefs.writeString(KEY_USER_ADDRESS, value)
        get() = prefs.readString(KEY_USER_ADDRESS, "")

    var userEthBalance: String
        set(value) = prefs.writeString(KEY_USER_ETH_BALANCE, value)
        get() = prefs.readString(KEY_USER_ETH_BALANCE, "0")

    var userTokenBalance: String
        set(value) = prefs.writeString(KEY_USER_TOKEN_BALANCE, value)
        get() = prefs.readString(KEY_USER_TOKEN_BALANCE, "0")

    var userStaked: String
        set(value) = prefs.writeString(KEY_USER_STAKED, value)
        get() = prefs.readString(KEY_USER_STAKED, "0")

    val userAddressLiveData: LiveData<String> =
        StringEncodedPrefsLiveData(prefs, KEY_USER_ADDRESS, "")
    val userEthBalanceLiveData: LiveData<String> =
        StringEncodedPrefsLiveData(prefs, KEY_USER_ETH_BALANCE, "0")
    val userTokenBalanceLiveData: LiveData<String> =
        StringEncodedPrefsLiveData(prefs, KEY_USER_TOKEN_BALANCE, "0")
    val userStakedLiveData: LiveData<String> =
        StringEncodedPrefsLiveData(prefs, KEY_USER_STAKED, "0")

    var tokenName: String
        set(value) = prefs.writeString(KEY_TOKEN_NAME, value)
        get() = prefs.readString(KEY_TOKEN_NAME, "")

    var tokenSymbol: String
        set(value) = prefs.writeString(KEY_TOKEN_SYMBOL, value)
        get() = prefs.readString(KEY_TOKEN_SYMBOL, "")

    var tokenContractAddress: String
        set(value) = prefs.writeString(KEY_TOKEN_CONTRACT_ADDRESS, value)
        get() = prefs.readString(KEY_TOKEN_CONTRACT_ADDRESS, "")

    var tokenDecimals: Int
        set(value) = prefs.writeInt(KEY_TOKEN_DECIMALS, value)
        get() = prefs.readInt(KEY_TOKEN_DECIMALS, 0)

    fun setUserInfo(userResponse: UserResponse) {
        userAddress = userResponse.address
        userEthBalance = userResponse.balance
        userStaked = userResponse.staked
    }

    fun setTokenInfo(tokenResponse: TokenResponse) {
        tokenName = tokenResponse.name
        tokenSymbol = tokenResponse.symbol
        tokenContractAddress = tokenResponse.contractAddress
        tokenDecimals = tokenResponse.decimals
        userTokenBalance = tokenResponse.balance
    }

    //password checking and hashing run on back thread
    fun checkPin(pin: String): Deferred<Boolean> =
        GlobalScope.async(context = Default) { checkpw(pin, pinHash) }

    fun checkPin(pin: String, onStart: () -> Unit, onChecked: (Boolean) -> Unit) {
        onStart()
        JobBuilder {
            val result = checkPin(pin).await()
            withContext(Dispatchers.Main) {
                onChecked(result)
            }
        }.handleError {
            Log.d("<<SS", "Error occurred", it)
        }.run(jobComposite)
    }

    //async save of password cos hashing process takes quite a time
    fun setPin(pin: String): Deferred<Unit> = GlobalScope.async {
        pinHash = BCrypt.hashpw(pin, BCrypt.gensalt())
    }

    fun hasPin(): Boolean = prefs.hasKey(KEY_PIN)
}