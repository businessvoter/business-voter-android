package com.businessvoter.ui.address

import android.app.AlertDialog
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.MenuItem
import androidx.lifecycle.ViewModelProvider
import com.businessvoter.R
import com.businessvoter.model.LoginInfo
import com.businessvoter.model.UserInfo
import com.businessvoter.ui.base.BaseVMActivity
import com.businessvoter.ui.pin.EnterPinActivity
import com.businessvoter.util.extentions.bindTextChange
import com.businessvoter.util.extentions.setOnClickThrottled
import kotlinx.android.synthetic.main.activity_enter_token_contract.*

class EnterTokenContractActivity :
    BaseVMActivity<EnterTokenContractViewModel, EnterTokenContractNavigator>() {

    companion object {
        private val KEY_USER_INFO = "KEY_USER_INFO"

        fun getIntent(context: Context, userInfo: UserInfo) =
            Intent(context, EnterTokenContractActivity::class.java)
                .putExtra(KEY_USER_INFO, userInfo)
    }

    override fun initializeView() {
        setContentView(R.layout.activity_enter_token_contract)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setSupportActionBar(toolbar)

        viewModel.userInfo = intent.getParcelableExtra(KEY_USER_INFO) ?: return
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean =
        when (item.itemId) {
            android.R.id.home -> onBackPressed().let { true }
            else -> super.onOptionsItemSelected(item)
        }

    override fun bindViews() {
        super.bindViews()
        enterTokenContractEditText.bindTextChange(this, viewModel.tokenContractAddress)
    }

    override fun bindClicksAndListeners() {
        super.bindClicksAndListeners()
        enterTokenContractBtnContinue.setOnClickThrottled {
            viewModel.onContinueClick()
        }
    }

    override fun provideViewModel(): EnterTokenContractViewModel =
        ViewModelProvider(this).get(EnterTokenContractViewModel::class.java)

    override fun provideNavigator(): EnterTokenContractNavigator =
        object : EnterTokenContractNavigator {
            override fun showError(errorText: String) {
                AlertDialog.Builder(this@EnterTokenContractActivity)
                    .setTitle(R.string.common_error)
                    .setMessage(errorText)
                    .setPositiveButton(R.string.common_ok, null)
                    .show()
            }

            override fun goToEnterPin(loginInfo: LoginInfo) {
                startActivity(
                    EnterPinActivity.getIntent(
                        this@EnterTokenContractActivity,
                        loginInfo
                    )
                )
            }

        }
}