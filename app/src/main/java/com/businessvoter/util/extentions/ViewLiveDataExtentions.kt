package com.businessvoter.util.extentions

import android.view.View
import android.widget.EditText
import android.widget.TextView
import androidx.core.widget.doOnTextChanged
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.businessvoter.util.view.ProgressFrameLayout

fun ProgressFrameLayout.bindProgress(
    owner: LifecycleOwner,
    progressModel: ProgressModeLiveData,
    showProgressByDefault: Boolean = false
) {
    setProgressShown(showProgressByDefault, false)
    progressModel.observe(
        owner,
        Observer { setProgressShown(it == ProgressModeLiveData.PROGRESS, true) })
}

fun TextView.bindText(owner: LifecycleOwner, textLive: LiveData<String>, default: String = "") {
    text = textLive.value ?: default
    textLive.observe(owner, Observer { text = it })
}

fun EditText.bindTextChange(
    owner: LifecycleOwner,
    textLive: MutableLiveData<String>,
    doOnChange: ((String) -> Unit)? = null
) {
    textLive.observe(owner, Observer { text: String ->
        setTailText(text)
    })
    doOnTextChanged { text, _, _, _ ->
        text?.toString()?.let {
            doOnChange?.invoke(it) ?: textLive.set(it)
        }
    }
}

fun View.bindVisibility(
    owner: LifecycleOwner,
    isVisibleLive: LiveData<Boolean>,
    default: Boolean = true
) {
    this.bindVisibility(
        owner,
        isVisibleLive.map { if (it) View.VISIBLE else View.GONE },
        if (default) View.VISIBLE else View.GONE
    )
}

fun View.bindVisibility(
    owner: LifecycleOwner,
    visibilityLive: LiveData<Int>,
    default: Int = View.VISIBLE
) {
    visibility = visibilityLive.value ?: default
    visibilityLive.observe(owner, Observer { visibility = it })
}

fun SwipeRefreshLayout.bindRefreshing(
    owner: LifecycleOwner,
    isRefreshing: LiveData<Boolean>,
    default: Boolean = false
) {
    setRefreshing(isRefreshing.value ?: default)
    isRefreshing.observe(owner, Observer { setRefreshing(it) })
}