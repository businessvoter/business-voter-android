package com.businessvoter.ui.key.new

import androidx.lifecycle.MutableLiveData
import com.businessvoter.R
import com.businessvoter.model.UserInfo
import com.businessvoter.ui.base.BaseViewModel
import com.businessvoter.util.EthHelper
import com.businessvoter.util.EthHelper.generatePrivateKey
import com.businessvoter.util.extentions.bindString
import com.businessvoter.util.extentions.get
import com.businessvoter.util.extentions.set

class GenerateKeyViewModel : BaseViewModel<GenerateKeyNavigator>() {
    val generatedPrivateKey: MutableLiveData<String> = MutableLiveData("")

    init {
        onGenerateClick()
    }

    fun onGenerateClick() {
        generatedPrivateKey.set(generatePrivateKey())
    }

    fun onContinueClick() {
        try {
            val validInfo: UserInfo = EthHelper.getInfoFromPrivateKey(generatedPrivateKey.get())
            navigator?.goToTokenAddressInput(validInfo)
        } catch (e: Exception) {
            navigator?.showError(bindString(R.string.common_error_invalid_pk))
        }
    }
}