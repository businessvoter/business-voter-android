package com.businessvoter.ui.main

import android.view.LayoutInflater
import android.view.ViewGroup
import com.businessvoter.R
import com.businessvoter.model.ui.PollUI
import com.businessvoter.ui.base.BaseRecyclerAdapter
import com.businessvoter.ui.base.BaseViewHolder

class MainPollAdapter : BaseRecyclerAdapter<PollUI>() {
    override fun provideViewHolder(
        layoutInflater: LayoutInflater,
        parent: ViewGroup,
        viewType: Int
    ): BaseViewHolder<PollUI> =
        MainPollViewHolder(layoutInflater.inflate(R.layout.item_poll_staked, parent, false))
}