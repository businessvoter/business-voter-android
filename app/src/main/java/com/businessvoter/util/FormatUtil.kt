package com.businessvoter.util

import com.businessvoter.prefs.UserPrefs
import com.businessvoter.util.extentions.format
import java.math.BigDecimal
import kotlin.math.min

const val MAX_DISPLAY_PRECISION: Int = 8

fun formatAmount(balance: String): String =
    "${balance.toBigDecimalSafe().let {
        if (it < BigDecimal.ONE.divide(BigDecimal.TEN.pow(MAX_DISPLAY_PRECISION))
            && it > BigDecimal.ZERO
        )
            "<0.${"0".repeat(MAX_DISPLAY_PRECISION - 1)}1"
        else
            it.format(provideFormat(min(UserPrefs.tokenDecimals, MAX_DISPLAY_PRECISION)))
    }} ${UserPrefs.tokenSymbol}"

private fun provideFormat(precision: Int): String = (0 until precision).fold("0") { acc, i ->
    acc + when (i) {
        0 -> ".#"
        else -> "#"
    }
}