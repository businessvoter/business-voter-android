package com.businessvoter.ui.vote

import android.app.Activity
import android.app.AlertDialog
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.MenuItem
import androidx.lifecycle.ViewModelProvider
import com.businessvoter.R
import com.businessvoter.model.db.Answer
import com.businessvoter.model.db.Poll
import com.businessvoter.ui.base.BaseVMActivity
import com.businessvoter.ui.pin.EnterPinActivity
import com.businessvoter.ui.success.SuccessActivity
import com.businessvoter.util.extentions.*
import kotlinx.android.synthetic.main.activity_vote.*

class VoteActivity : BaseVMActivity<VoteViewModel, VoteNavigator>() {
    companion object {
        private val KEY_POLL = "KEY_POLL"
        private val KEY_ANSWER = "KEY_ANSWER"

        private const val CONFIRM_VOTE_REQUEST_CODE: Int = 636

        fun getIntent(context: Context, poll: Poll, answer: Answer) =
            Intent(context, VoteActivity::class.java)
                .putExtra(KEY_POLL, poll)
                .putExtra(KEY_ANSWER, answer)
    }

    override fun initializeView() {
        setContentView(R.layout.activity_vote)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setSupportActionBar(toolbar)

        viewModel.poll.set(intent.getParcelableExtra(KEY_POLL) ?: return)
        viewModel.answer.set(intent.getParcelableExtra(KEY_ANSWER) ?: return)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean =
        when (item.itemId) {
            android.R.id.home -> onBackPressed().let { true }
            else -> super.onOptionsItemSelected(item)
        }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (resultCode != Activity.RESULT_OK) return
        when (requestCode) {
            CONFIRM_VOTE_REQUEST_CODE -> {
                viewModel.onVoteConfirmed()
            }
            else -> super.onActivityResult(requestCode, resultCode, data)
        }
    }

    override fun bindViews() {
        super.bindViews()
        viewModel.let {
            voteProgress.bindProgress(this, it.progressMode)
            voteTxtDescription.bindText(this, it.description)
            voteEditText.bindTextChange(this, it.amount)
        }
    }

    override fun bindClicksAndListeners() {
        super.bindClicksAndListeners()
        voteBtnVote.setOnClickThrottled {
            viewModel.onVoteClick()
        }
    }

    override fun provideViewModel(): VoteViewModel =
        ViewModelProvider(this).get(VoteViewModel::class.java)

    override fun provideNavigator(): VoteNavigator = object : VoteNavigator {
        override fun showError(errorText: String) {
            AlertDialog.Builder(this@VoteActivity)
                .setTitle(R.string.common_error)
                .setMessage(errorText)
                .setPositiveButton(R.string.common_ok, null)
                .show()
        }

        override fun goToConfirmPin(confirmText: String) {
            startActivityForResult(
                EnterPinActivity.getIntent(this@VoteActivity, confirmText),
                CONFIRM_VOTE_REQUEST_CODE
            )
        }

        override fun goToSuccess(successText: String, txId: String) {
            startActivity(
                SuccessActivity.getIntent(this@VoteActivity, successText, txId)
            )
            finish()
        }
    }
}