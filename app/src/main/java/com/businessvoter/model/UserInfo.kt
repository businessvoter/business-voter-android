package com.businessvoter.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class UserInfo(val address: String, val signature: String, val privateKey: String) : Parcelable