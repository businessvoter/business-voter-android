package com.businessvoter.ui.details

import android.app.Activity
import android.app.AlertDialog
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.MenuItem
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.businessvoter.R
import com.businessvoter.model.db.Answer
import com.businessvoter.model.db.Poll
import com.businessvoter.ui.base.BaseVMActivity
import com.businessvoter.ui.pin.EnterPinActivity
import com.businessvoter.ui.success.SuccessActivity
import com.businessvoter.ui.vote.VoteActivity
import com.businessvoter.util.extentions.*
import com.businessvoter.util.view.OrderedVerticalAnswerItem
import kotlinx.android.synthetic.main.activity_poll_details.*

class PollDetailsActivity : BaseVMActivity<PollDetailsViewModel, PollDetailsNavigator>() {
    companion object {
        private val KEY_POLL_ID = "KEY_POLL_ID"
        private const val CONFIRM_UNSTAKE_REQUEST_CODE: Int = 74

        fun getIntent(context: Context, pollId: Long) =
            Intent(context, PollDetailsActivity::class.java)
                .putExtra(KEY_POLL_ID, pollId)
    }

    override fun initializeView() {
        setContentView(R.layout.activity_poll_details)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setSupportActionBar(toolbar)

        viewModel.pollId.set(intent.getLongExtra(KEY_POLL_ID, 0))
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean =
        when (item.itemId) {
            android.R.id.home -> onBackPressed().let { true }
            else -> super.onOptionsItemSelected(item)
        }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (resultCode != Activity.RESULT_OK) return
        when (requestCode) {
            CONFIRM_UNSTAKE_REQUEST_CODE -> {
                viewModel.onUnstakeConfirmed()
            }
            else -> super.onActivityResult(requestCode, resultCode, data)
        }
    }

    override fun bindViews() {
        super.bindViews()
        viewModel.let {
            pollDetailsProgress.bindProgress(this, it.progressMode)
            pollDetailsTxtQuestion.bindText(this, it.question)
            pollDetailsTxtEndDate.bindText(this, it.formattedEndDate)
            pollDetailsTxtTotalVotes.bindText(this, it.totalStaked)
            pollDetailsTxtTotalVotes.bindVisibility(this, it.isTotalStakedVisible)
            pollDetailsBtnUnstake.bindVisibility(this, it.isUnstakeVisible)

            viewModel.models.observe(this, Observer { data: List<OrderedVerticalAnswerItem> ->
                pollDetailsOrderedVerticalAnswers.setData(data = data)
            })
        }
    }

    override fun bindClicksAndListeners() {
        super.bindClicksAndListeners()
        pollDetailsBtnUnstake.setOnClickThrottled {
            viewModel.onUnstakeClick()
        }
    }

    override fun provideViewModel(): PollDetailsViewModel =
        ViewModelProvider(this).get(PollDetailsViewModel::class.java)

    override fun provideNavigator(): PollDetailsNavigator = object : PollDetailsNavigator {
        override fun goToVote(poll: Poll, answer: Answer) {
            startActivity(VoteActivity.getIntent(this@PollDetailsActivity, poll, answer))
        }

        override fun showError(errorText: String) {
            AlertDialog.Builder(this@PollDetailsActivity)
                .setTitle(R.string.common_error)
                .setMessage(errorText)
                .setPositiveButton(R.string.common_ok, null)
                .show()
        }

        override fun goToConfirmPin(confirmText: String) {
            startActivityForResult(
                EnterPinActivity.getIntent(this@PollDetailsActivity, confirmText),
                CONFIRM_UNSTAKE_REQUEST_CODE
            )
        }

        override fun goToSuccess(successText: String, txId: String) {
            startActivity(
                SuccessActivity.getIntent(this@PollDetailsActivity, successText, txId)
            )
        }
    }
}