package com.businessvoter.crypto

import com.businessvoter.util.EthHelper.toByteArray
import org.bouncycastle.util.encoders.Hex
import org.junit.Assert
import org.junit.Test
import org.web3j.crypto.Credentials
import org.web3j.crypto.ECKeyPair
import org.web3j.crypto.Hash
import org.web3j.crypto.Sign
import org.web3j.utils.Numeric
import org.web3j.utils.Numeric.hexStringToByteArray


class SignatureTest {
    @Test
    fun testSignature() {
        // Generate private key
        val privateKey: ByteArray = Hex.decode("7330532f25298dd8047f839d41a77de3dbd04d7048cbfbffd03d6daf1246b4d8")
        val credentials: Credentials = Credentials.create(ECKeyPair.create(privateKey))

        // Generate address
        val address: String = credentials.address
        Assert.assertEquals("0x54e19Dafb300062279ecDb05D2680c78Cd8f02Cd".toLowerCase(), address)

        // Generate signature hash
        val signatureHash: ByteArray = Hash.sha3(hexStringToByteArray(address))
        Assert.assertEquals("0x4dae7a3de5d122525092a89026e97f00c35ae0b1686c59e98d583fae8388132c", Numeric.toHexString(signatureHash))

        // Sign with private key
        val signature: Sign.SignatureData = Sign.signMessage(signatureHash, credentials.ecKeyPair, false)
        Assert.assertEquals("0xc0f38236f4a3de1e9db403d7248fead7f004cc07bf431a774e673a1b21a8e57c0042bfd0e93e196ad03bb6d210caa31bb2466c2320d2692920d9409d550d748900", Numeric.toHexString(toByteArray(signature)))
    }
}