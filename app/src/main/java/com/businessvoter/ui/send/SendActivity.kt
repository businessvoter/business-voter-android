package com.businessvoter.ui.send

import android.app.Activity
import android.app.AlertDialog
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.MenuItem
import androidx.lifecycle.ViewModelProvider
import com.businessvoter.R
import com.businessvoter.ui.base.BaseVMActivity
import com.businessvoter.ui.pin.EnterPinActivity
import com.businessvoter.ui.success.SuccessActivity
import com.businessvoter.util.extentions.bindProgress
import com.businessvoter.util.extentions.bindTextChange
import com.businessvoter.util.extentions.setOnClickThrottled
import kotlinx.android.synthetic.main.activity_send.*

class SendActivity : BaseVMActivity<SendViewModel, SendNavigator>() {
    companion object {
        private const val CONFIRM_SEND_REQUEST_CODE: Int = 34

        fun getIntent(context: Context) =
            Intent(context, SendActivity::class.java)
    }

    override fun initializeView() {
        setContentView(R.layout.activity_send)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setSupportActionBar(toolbar)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean =
        when (item.itemId) {
            android.R.id.home -> onBackPressed().let { true }
            else -> super.onOptionsItemSelected(item)
        }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (resultCode != Activity.RESULT_OK) return
        when (requestCode) {
            CONFIRM_SEND_REQUEST_CODE -> {
                viewModel.onSendConfirmed()
            }
            else -> super.onActivityResult(requestCode, resultCode, data)
        }
    }

    override fun bindViews() {
        super.bindViews()
        viewModel.let {
            sendProgress.bindProgress(this, it.progressMode)
            sendEditTextTo.bindTextChange(this, it.to)
            sendEditTextAmount.bindTextChange(this, it.amount)
        }
    }

    override fun bindClicksAndListeners() {
        super.bindClicksAndListeners()
        sendBtnSend.setOnClickThrottled {
            viewModel.onSendClick()
        }
    }

    override fun provideViewModel(): SendViewModel =
        ViewModelProvider(this).get(SendViewModel::class.java)

    override fun provideNavigator(): SendNavigator = object : SendNavigator {
        override fun showError(errorText: String) {
            AlertDialog.Builder(this@SendActivity)
                .setTitle(R.string.common_error)
                .setMessage(errorText)
                .setPositiveButton(R.string.common_ok, null)
                .show()
        }

        override fun goToConfirmPin(confirmText: String) {
            startActivityForResult(
                EnterPinActivity.getIntent(this@SendActivity, confirmText),
                CONFIRM_SEND_REQUEST_CODE
            )
        }

        override fun goToSuccess(successText: String, txId: String) {
            startActivity(
                SuccessActivity.getIntent(this@SendActivity, successText, txId)
            )
            finish()
        }
    }

}