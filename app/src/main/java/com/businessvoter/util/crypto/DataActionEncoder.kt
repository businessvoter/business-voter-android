package com.businessvoter.util.crypto

import org.web3j.abi.FunctionEncoder
import org.web3j.abi.datatypes.Address
import org.web3j.abi.datatypes.generated.Uint256
import java.math.BigInteger

object DataActionEncoder {

    fun encodeTransferData(
        toAddress: String,
        value: BigInteger
    ): String? {
        val function: org.web3j.abi.datatypes.Function = org.web3j.abi.datatypes.Function(
            "transfer",
            listOf(
                Address(toAddress),
                Uint256(value)
            ),
            emptyList()
        )
        return FunctionEncoder.encode(function)
    }

    fun encodeStakeData(
        erc20TokenAddress: String,
        value: BigInteger,
        pollId: BigInteger,
        answerId: BigInteger
    ): String? {
        val function: org.web3j.abi.datatypes.Function = org.web3j.abi.datatypes.Function(
            "stake",
            listOf(
                Address(erc20TokenAddress),
                Uint256(value),
                Uint256(pollId),
                Uint256(answerId)
            ),
            emptyList()
        )
        return FunctionEncoder.encode(function)
    }

    fun encodeUnstakeData(
        erc20TokenAddress: String,
        pollId: BigInteger
    ): String? {
        val function: org.web3j.abi.datatypes.Function = org.web3j.abi.datatypes.Function(
            "unstake",
            listOf(
                Address(erc20TokenAddress),
                Uint256(pollId)
            ),
            emptyList()
        )
        return FunctionEncoder.encode(function)
    }
}