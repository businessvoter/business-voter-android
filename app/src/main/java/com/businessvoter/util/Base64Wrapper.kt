package com.businessvoter.util

import org.bouncycastle.util.encoders.Base64

fun fromBase64(base64Str: String): ByteArray = Base64.decode(base64Str)

fun fromBase64AsString(base64Str: String): String = String(fromBase64(base64Str))

fun toBase64(byteArray: ByteArray): String = Base64.toBase64String(byteArray)

fun strToBase64(string: String): String = toBase64(string.toByteArray())