package com.businessvoter.util.view

import android.animation.ValueAnimator
import android.view.ViewPropertyAnimator

fun ViewPropertyAnimator.bag(list: MutableList<Anim>) {
    list.add(toAnim())
}

fun ValueAnimator.bag(list: MutableList<Anim>) {
    list.add(toAnim())
}

fun MutableList<Anim>.cancelAll() {
    forEach { it.cancel() }
    clear()
}

fun ViewPropertyAnimator.toAnim() = Anim(this)

fun ValueAnimator.toAnim() = Anim(this)

class Anim() {

    var viewPropertyAnimator: ViewPropertyAnimator? = null
    var valueAnimator: ValueAnimator? = null

    constructor(viewPropertyAnimator: ViewPropertyAnimator) : this() {
        this.viewPropertyAnimator = viewPropertyAnimator
    }

    constructor(valueAnimator: ValueAnimator) : this() {
        this.valueAnimator = valueAnimator
    }

    fun cancel() {
        valueAnimator?.cancel()
        viewPropertyAnimator?.cancel()
    }
}