package com.businessvoter.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class LoginInfo(
    val address: String,
    val signature: String,
    val tokenAddress: String,
    val privateKey: String
) : Parcelable