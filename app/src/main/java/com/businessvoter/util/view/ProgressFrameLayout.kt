package com.businessvoter.util.view

import android.animation.ObjectAnimator
import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.LinearInterpolator
import android.widget.FrameLayout
import com.businessvoter.R

open class ProgressFrameLayout @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyle: Int = 0
) :
    FrameLayout(context, attrs, defStyle) {

    private val progressLayoutResId: Int
    val containerView: FrameLayout
    private val progressView: View
    private val animator: ProgressAnimator

    var statusBoard: StatusBoard? = null

    var lastState: Boolean = false

    init {
        val a = getContext().theme.obtainStyledAttributes(
            attrs,
            R.styleable.ProgressFrameLayout,
            0,
            0
        )

        val duration: Int

        try {
            progressLayoutResId = a.getResourceId(R.styleable.ProgressFrameLayout_progressLayout, 0)
            duration = a.getInt(R.styleable.ProgressFrameLayout_progressDuration, 200)
            assertProgressLayoutNotNull()
        } finally {
            a.recycle()
        }

        containerView = FrameLayout(getContext())
        containerView.layoutParams = FrameLayout.LayoutParams(
            ViewGroup.LayoutParams.MATCH_PARENT,
            ViewGroup.LayoutParams.MATCH_PARENT
        )
        progressView = LayoutInflater.from(getContext()).inflate(progressLayoutResId, this, false)
        statusBoard = progressView.findViewById(R.id.statusBoard)
        createCircleAnimators(progressView.findViewById(R.id.inner_circle), 0F, 360F)
        createCircleAnimators(progressView.findViewById(R.id.outer_circle), 720F, 360F)

        addView(containerView, 0)
        addView(progressView, 1)

        animator = ProgressAnimator(containerView, progressView, duration)
    }

    override fun addView(child: View?, index: Int, params: ViewGroup.LayoutParams?) {
        if (child == progressView || child == containerView) {
            super.addView(child, index, params)
        } else {
            containerView.addView(child, index, params)
        }
    }

    @JvmOverloads
    fun setProgressShown(shown: Boolean, animate: Boolean = true) {
        if (shown == lastState) return
        lastState = shown
        if (shown) {
            animator.showProgress(animate)
        } else {
            animator.hideProgress(animate)
            statusBoard?.clear()
        }
    }

    fun updateTextProgress(status: String) {
        status.takeIf {
            it.isNotBlank()
        }?.let {
            statusBoard?.updateStatus(it)
        }
    }

    private fun assertProgressLayoutNotNull() {
        if (progressLayoutResId == 0)
            throw IllegalArgumentException("xml progressLayout parameter of ProgressFrameLayout cannot be null")
    }

    private fun createCircleAnimators(target: View, from: Float, to: Float) {
        ObjectAnimator.ofFloat(target, "rotation", from, to).apply {
            repeatCount = ObjectAnimator.INFINITE
            interpolator = LinearInterpolator()
            duration = 1200
        }.start()
    }
}
