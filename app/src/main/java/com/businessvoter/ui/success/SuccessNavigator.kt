package com.businessvoter.ui.success

import com.businessvoter.ui.base.BaseNavigator

interface SuccessNavigator : BaseNavigator {
    fun navigateToWeb(url: String)
}