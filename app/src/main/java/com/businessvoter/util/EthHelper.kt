package com.businessvoter.util

import com.businessvoter.model.UserInfo
import com.paytomat.core.util.ByteSerializer
import org.bouncycastle.util.encoders.Hex
import org.web3j.crypto.Credentials
import org.web3j.crypto.ECKeyPair
import org.web3j.crypto.Hash
import org.web3j.crypto.Sign
import org.web3j.utils.Numeric
import java.security.SecureRandom

object EthHelper {
    fun getInfoFromPrivateKey(pk: String): UserInfo {
        try {
            val privateKey: ByteArray = Hex.decode(pk)
            val credentials: Credentials = Credentials.create(ECKeyPair.create(privateKey))
            val address: String = credentials.address
            val signatureHash: ByteArray = Hash.sha3(Numeric.hexStringToByteArray(address))
            val signature: Sign.SignatureData =
                Sign.signMessage(signatureHash, credentials.ecKeyPair, false)
            return UserInfo(address, Numeric.toHexString(toByteArray(signature)), pk)
        } catch (e: Exception) {
            throw Exception()
        }
    }

    fun generatePrivateKey(): String {
        val random = SecureRandom()
        val bytes = ByteArray(32)
        random.nextBytes(bytes)
        return Numeric.toHexStringNoPrefix(bytes)
    }

    fun toByteArray(signature: Sign.SignatureData): ByteArray? {
        return ByteSerializer.create()
            .write(signature.r, 0, 32)
            .write(signature.s, 0, 32)
            .write(0.toByte())
            .serialize()
    }
}