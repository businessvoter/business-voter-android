package com.businessvoter.ui.polls

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.businessvoter.db.dao.PollDao
import com.businessvoter.model.ui.PollUI
import com.businessvoter.repository.UserRepository
import com.businessvoter.ui.base.BaseViewModel
import com.businessvoter.ui.base.empty.EmptyNavigator
import com.businessvoter.util.extentions.JobBuilder
import com.businessvoter.util.extentions.map
import com.businessvoter.util.extentions.set

class PollsViewModel : BaseViewModel<EmptyNavigator>() {
    val isRefreshing: MutableLiveData<Boolean> = MutableLiveData(false)

    val models: LiveData<List<PollUI>> = PollDao.db.getPolls().map {
        it.map { PollUI(it) }
    }

    val isRecyclerVisible: LiveData<Boolean> = models.map { it.isNotEmpty() }

    val isEmptyViewVisible: LiveData<Boolean> = models.map { it.isEmpty() }

    fun onRefresh() {
        isRefreshing.set(true)
        JobBuilder {
            UserRepository.getUserInfo()
        }.finally {
            isRefreshing.set(false)
        }.runSafe(jobComposite)
    }
}