package com.businessvoter.ui.polls

import android.view.View
import com.businessvoter.model.ui.PollUI
import com.businessvoter.ui.base.BaseViewHolder
import kotlinx.android.synthetic.main.item_poll.view.*

class PollViewHolder(view: View) : BaseViewHolder<PollUI>(view) {
    override fun bind(model: PollUI) {
        itemView.apply {
            itemPollQuestion.text = model.question
            itemPollFormattedEndTime.text = model.getFormattedEndTime()
            setOnClickListener { clickListener?.onClick(adapterPosition, model) }
        }
    }
}