package com.businessvoter.model.db

import android.os.Parcelable
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.businessvoter.model.api.PollResponse
import com.businessvoter.util.extentions.msToSec
import kotlinx.android.parcel.Parcelize

@Parcelize
@Entity(tableName = "poll")
data class Poll(
    @PrimaryKey val id: Long,
    val created: Long,
    val endTime: Long,
    val denominator: String,
    val numerator: String,
    val text: String,
    val totalStaked: String,
    val unstaked: Boolean,
    val userStaked: String
) : Parcelable {
    constructor(response: PollResponse) : this(
        response.id,
        response.created,
        response.endTime,
        response.interest.denominator,
        response.interest.numerator,
        response.text,
        response.totalStaked,
        response.unstaked,
        response.userStaked
    )

    fun isFinished(): Boolean = endTime < System.currentTimeMillis().msToSec()
}