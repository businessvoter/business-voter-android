package com.businessvoter.util.extentions

import android.os.Looper
import androidx.annotation.IntDef
import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import com.businessvoter.util.extentions.ProgressModeLiveData.Companion.CONTENT
import com.businessvoter.util.extentions.ProgressModeLiveData.Companion.PROGRESS

@IntDef(CONTENT, PROGRESS)
@Retention(AnnotationRetention.SOURCE)
annotation class ProgressMode

class ProgressModeLiveData(@ProgressMode defaultMode: Int = CONTENT) :
    MutableLiveData<Int>(defaultMode) {

    companion object {
        const val CONTENT = 0
        const val PROGRESS = 1
    }

    fun content() {
        set(CONTENT)
    }

    fun progress() {
        set(PROGRESS)
    }

    fun isInProgress(): Boolean = get() == PROGRESS
}

fun <T1, T2> LiveData<T1>.flatMap(function: (T1) -> LiveData<T2>): LiveData<T2> =
    Transformations.switchMap(this, function)

fun <T1, T2> LiveData<T1>.map(function: (T1) -> T2): LiveData<T2> =
    Transformations.map(this, function)

fun <T1 : Any, T2 : Any> LiveData<T1>.zip(source2: LiveData<T2>): LiveData<Pair<T1, T2>> =
    MediatorLiveData<Pair<T1, T2>>().apply {
        addSource(this@zip) { data ->
            value = (data ?: return@addSource) to (source2.value ?: return@addSource)
        }
        addSource(source2) { data ->
            value = (this@zip.value ?: return@addSource) to (data ?: return@addSource)
        }
    }

fun <T1 : Any, T2 : Any, T3 : Any> LiveData<T1>.zip(
    source2: LiveData<T2>,
    source3: LiveData<T3>
): LiveData<Triple<T1, T2, T3>> =
    MediatorLiveData<Triple<T1, T2, T3>>().apply {
        addSource(this@zip.zip(source2)) { (data1, data2) ->
            value = Triple(
                data1,
                data2,
                (source3.value ?: return@addSource)
            )
        }
        addSource(source3) { data ->
            value = Triple(
                (this@zip.value ?: return@addSource),
                (source2.value ?: return@addSource),
                data ?: return@addSource
            )
        }
    }

fun LiveData<Boolean>.get(default: Boolean = false): Boolean = this.value ?: default

fun LiveData<Int>.get(default: Int = 0): Int = this.value ?: default

fun LiveData<Double>.get(default: Double = 0.0): Double = this.value ?: default

fun LiveData<Long>.get(default: Long = 0): Long = this.value ?: default

fun LiveData<String>.get(default: String = ""): String = this.value ?: default

fun <T> MutableLiveData<T>.set(value: T?) {
    if (Looper.myLooper() == Looper.getMainLooper()) setValue(value)
    else postValue(value)
}