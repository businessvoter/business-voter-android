package com.businessvoter.ui.base

import androidx.recyclerview.widget.DiffUtil
import com.businessvoter.util.extentions.JobBuilder
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

object DiffList {

    fun <VH : BaseViewHolder<M>, M : DiffCompared> merge(
        adapter: BaseRecyclerAdapter<M>,
        update: List<M>,
        doAfter: () -> Unit
    ) {
        val old: List<M> = adapter.modelsList.toList()
        JobBuilder {
            val diffResult: DiffUtil.DiffResult = DiffUtil.calculateDiff(DiffCallback(old, update))
            withContext(Dispatchers.Main) {
                adapter.modelsList.apply {
                    clear()
                    addAll(update)
                }
                diffResult.dispatchUpdatesTo(adapter)
                doAfter()
            }
        }.runSafe()
    }
}

class DiffCallback(val oldList: List<DiffCompared>, val newList: List<DiffCompared>) :
    DiffUtil.Callback() {

    override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int) =
        oldList[oldItemPosition].idEquals(newList[newItemPosition])

    override fun getOldListSize(): Int = oldList.size

    override fun getNewListSize(): Int = newList.size

    override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int) =
        oldList[oldItemPosition].uiEquals(newList[newItemPosition])
}