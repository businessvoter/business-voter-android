package com.businessvoter.ui.address

import com.businessvoter.model.LoginInfo
import com.businessvoter.ui.base.BaseNavigator

interface EnterTokenContractNavigator : BaseNavigator {
    fun showError(errorText: String)
    fun goToEnterPin(loginInfo: LoginInfo)
}