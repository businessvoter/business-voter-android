package com.businessvoter.util.extentions

import org.json.JSONException
import org.json.JSONObject

class JSONKey(val key: String, val parent: JSONObject) {

    val boolean
        get() = boolean()

    val int
        get() = int()

    val long
        get() = long()

    val float
        get() = float()

    val double
        get() = double()

    val string
        get() = string()

    val nullableString
        get() = parent.optString(key)

    fun string(defaultValue: String = ""): String = parent.optString(key, defaultValue)

    fun boolean(defaultValue: Boolean = false) = parent.optBoolean(key, defaultValue)

    fun int(defaultValue: Int = 0) = parent.optInt(key, defaultValue)

    fun long(defaultValue: Long = 0) = parent.optLong(key, defaultValue)

    fun double(defaultValue: Double = 0.0) = parent.optDouble(key, defaultValue)

    fun float(defaultValue: Float = 0.0f) = double(defaultValue.toDouble()).toFloat()

}


class JSONKt : JSONObject {

    @Throws(JSONException::class)
    private constructor(json: String?) : super(json)

    constructor(map: Map<*, *>) : super(map)

    companion object {
        fun parse(json: String?): JSONKt? {
            try {
                if (!json.isNullOrBlank())
                    return JSONKt(json)
            } catch (e: Exception) {
            }
            return null
        }
    }

    override operator fun get(key: String): JSONKey = JSONKey(key, this)
}