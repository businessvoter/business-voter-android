package com.businessvoter.api

import com.businessvoter.model.api.*
import kotlinx.coroutines.Deferred
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST

interface BusinessVoterService {
    @POST("api/v1/user/login")
    fun loginUser(@Body userLoginPostRequest: UserLoginPostRequest): Deferred<UserLoginPostResponse>

    @GET("api/v1/user")
    fun getUserInfo(): Deferred<UserResponse>

    @GET("api/v1/user/eth/gas_price")
    fun getGasPrice(): Deferred<EthGasPriceResponse>

    @GET("api/v1/user/eth/nonce")
    fun getNonce(): Deferred<EthNonceResponse>

    @POST("api/v1//user/eth/transaction")
    fun postTransaction(@Body ethPostTransactionRequestBody: EthPostTransactionRequestBody): Deferred<EthPostTransactionResponse>

}