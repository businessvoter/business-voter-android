package com.businessvoter.ui.base

interface DiffCompared {
    fun idEquals(obj: Any): Boolean
    fun uiEquals(obj: Any): Boolean
}