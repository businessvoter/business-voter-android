package com.businessvoter.ui.pin

import androidx.annotation.IntDef
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.businessvoter.R
import com.businessvoter.api.ApiException
import com.businessvoter.model.LoginInfo
import com.businessvoter.prefs.UserPrefs
import com.businessvoter.repository.UserRepository
import com.businessvoter.ui.base.BaseViewModel
import com.businessvoter.ui.pin.EnterPinViewModel.Companion.MODE_ENTER_TO_SEND
import com.businessvoter.ui.pin.EnterPinViewModel.Companion.MODE_NEW
import com.businessvoter.ui.pin.EnterPinViewModel.Companion.MODE_UNSET
import com.businessvoter.util.extentions.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

@IntDef(MODE_ENTER_TO_SEND, MODE_NEW, MODE_UNSET)
annotation class PinMode

class EnterPinViewModel : BaseViewModel<EnterPinNavigator>() {
    companion object {
        const val MODE_ENTER_TO_SEND: Int = 0
        const val MODE_NEW: Int = 2
        const val MODE_UNSET: Int = 4
    }

    val progressMode: ProgressModeLiveData = ProgressModeLiveData()

    val loginInfo: MutableLiveData<LoginInfo?> = MutableLiveData(null)
    private val mode: LiveData<Int> = loginInfo.map {
        if (it != null) MODE_NEW else MODE_ENTER_TO_SEND
    }

    val description: MutableLiveData<String> = MutableLiveData("")

    val isDescriptionVisible: LiveData<Boolean> = description.map { !it.isNullOrBlank() }

    val pin: MutableLiveData<String> = MutableLiveData("")
    val pinConfirmation: MutableLiveData<String> = MutableLiveData("")

    val isPinConfirmationVisible: LiveData<Boolean> = mode.map { it == MODE_NEW }

    val buttonText: LiveData<String> = mode.map {
        bindString(
            if (it == MODE_NEW) R.string.enter_pin_login
            else R.string.enter_pin_confirm
        )
    }

    fun onBtnClick() {
        if (mode.get() == MODE_NEW) onNewMode()
        else onSendMode()
    }

    private fun onNewMode() {
        val loginInfo: LoginInfo = loginInfo.value ?: return
        val pin: String = pin.get()
        val pinConfirmation: String = pinConfirmation.get()

        val error: String = when {
            pin.isBlank() -> bindString(R.string.common_error_invalid_pin_empty)
            pin != pinConfirmation -> bindString(R.string.common_error_invalid_pin_confirm)
            else -> ""
        }
        if (error.isNotBlank()) {
            navigator?.showError(error)
            return
        }
        encrypt(pin, {
            progressMode.progress()
        }, {
            JobBuilder {
                UserRepository.postUserInfo(loginInfo)
                navigator?.goToMainScreen()
            }.handleError {
                navigator?.showError(it.localizedMessage ?: bindString(R.string.common_error))
            }.finally {
                progressMode.content()
            }.run(jobComposite)
        }, {
            navigator?.showError(it.localizedMessage ?: bindString(R.string.common_error))
            progressMode.content()
        })
    }

    private fun onSendMode() {
        val pin: String = pin.get()
        UserPrefs.checkPin(pin, {
            progressMode.progress()
        }, {
            if (it) {
                navigator?.goBackWithSuccess()
            } else {
                progressMode.content()
                navigator?.showError(bindString(R.string.common_error_invalid_pin))
            }
        })
    }

    private fun encrypt(
        pin: String,
        onStart: () -> Unit,
        onComplete: () -> Unit,
        onError: (ApiException) -> Unit
    ) {
        onStart()
        JobBuilder {
            UserPrefs.setPin(pin).await()
            withContext(Dispatchers.Main) {
                onComplete()
            }
        }.handleError {
            onError(it as? ApiException ?: ApiException())
        }.run(jobComposite)
    }
}