package com.businessvoter.model.db

import android.os.Parcelable
import androidx.room.Entity
import com.businessvoter.model.api.AnswerResponse
import kotlinx.android.parcel.Parcelize

@Parcelize
@Entity(tableName = "answer", primaryKeys = ["id", "pollId"])
data class Answer(
    val id: Long,
    val pollId: Long,
    val text: String,
    val totalStaked: String,
    val userStaked: String
) : Parcelable {
    constructor(pollId: Long, response: AnswerResponse) : this(
        response.id,
        pollId,
        response.text,
        response.totalStaked,
        response.userStaked
    )
}