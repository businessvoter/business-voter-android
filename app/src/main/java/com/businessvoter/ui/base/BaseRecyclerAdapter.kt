package com.businessvoter.ui.base

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import java.util.*

abstract class BaseRecyclerAdapter<M : DiffCompared> : RecyclerView.Adapter<BaseViewHolder<M>>() {

    companion object {

        private const val DEFAULT_TYPE = 1
    }

    val modelsList: MutableList<M> = ArrayList()
    var clickListenerRef: RecyclerClickListener<M>? = null

    fun setClickListener(clickListener: RecyclerClickListener<M>?) {
        if (clickListenerRef == clickListener) return
        clickListenerRef = clickListener
        notifyDataSetChanged()
    }

    fun applyChanges(models: List<M>, onComplete: (() -> Unit) = {}) {
        DiffList.merge(this, models) {
            onComplete()
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseViewHolder<M> {
        val inflater = LayoutInflater.from(parent.context)
        return provideViewHolder(inflater, parent, viewType)
    }

    abstract fun provideViewHolder(
        layoutInflater: LayoutInflater,
        parent: ViewGroup,
        viewType: Int
    ): BaseViewHolder<M>

    open fun provideViewType(position: Int): Int = DEFAULT_TYPE

    override fun getItemViewType(position: Int): Int = provideViewType(position)

    open override fun onBindViewHolder(holder: BaseViewHolder<M>, position: Int) {
        if (position == modelsList.size) return
        bindHolder(holder, position, modelsList[position])
    }

    open fun bindHolder(holder: BaseViewHolder<M>, position: Int, model: M) {
        holder.bind(model, clickListenerRef)
    }

    open override fun getItemCount(): Int = modelsList.size

    override fun onViewRecycled(holder: BaseViewHolder<M>) {
        super.onViewRecycled(holder)
        holder.clearUp()
    }
}