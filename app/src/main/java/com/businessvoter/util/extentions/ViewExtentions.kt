package com.businessvoter.util.extentions

import android.view.View
import android.widget.EditText
import com.businessvoter.util.ClickThrottler

fun View.setOnClickThrottled(
    throttler: ClickThrottler = ClickThrottler(),
    onClick: (View) -> Unit
) {
    this.setOnClickListener { throttler.offer { onClick(it) } }
}

fun EditText.setTailText(str: String?) {
    if (str == text.toString()) return
    setText(str)
}