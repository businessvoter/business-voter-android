package com.businessvoter.api

import android.util.Log
import com.businessvoter.helper.loadTokenSync
import com.businessvoter.prefs.UserPrefs
import okhttp3.*

class AuthorizationHandler : Interceptor, Authenticator {

    override fun intercept(chain: Interceptor.Chain): Response {
        var request: Request = chain.request()
        val pathSegments: List<String> = request.url.pathSegments
        return when {
            "token" in pathSegments || request.header("Authorization") != null -> chain.proceed(
                request
            )
            else -> {
                try {
                    if (UserPrefs.accessToken.isBlank()) loadTokenSync()
                } catch (e: Exception) {
                    Log.e("<<SS", "Error occurred", e)
                }

                if (UserPrefs.accessToken.isNotBlank()) {
                    request = request.newBuilder()
                        .addHeader("Authorization", "${UserPrefs.accessToken}")
                        .build()
                }

                chain.proceed(request)
            }
        }

    }

    override fun authenticate(route: Route?, response: Response): Request? = try {
        loadTokenSync()
        response.request
            .newBuilder()
            .header("Authorization", "Bearer ${UserPrefs.accessToken}")
            .build()

    } catch (e: Exception) {
        null
    }
}