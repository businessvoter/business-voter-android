package com.businessvoter.api

import androidx.annotation.IntDef
import androidx.annotation.StringRes
import com.businessvoter.R
import com.businessvoter.util.extentions.JSONKt
import com.businessvoter.util.extentions.bindString
import retrofit2.Response

const val ERROR_CODE_UNKNOWN = 0
const val ERROR_CODE_NO_INTERNET = 1
const val ERROR_CODE_REQUEST_TIMEOUT = 2

const val ERROR_CODE_NO_CONTRACT_CODE = 1007

@Retention(AnnotationRetention.SOURCE)
@IntDef(
    ERROR_CODE_NO_INTERNET,
    ERROR_CODE_UNKNOWN,
    ERROR_CODE_REQUEST_TIMEOUT,
    ERROR_CODE_NO_CONTRACT_CODE
)
annotation class ApiExceptionType

typealias ApiExceptionCode = Int

class ApiException(
    @ApiExceptionType val code: ApiExceptionCode,
    private val throwableMessage: String,
    cause: Throwable? = null
) : Exception(throwableMessage, cause) {

    companion object {

        fun parse(response: Response<*>?): ApiException {
            require(!(response == null || response.isSuccessful))
            val json = JSONKt.parse(response.errorBody()?.string())
                ?: return ApiException(ERROR_CODE_UNKNOWN)
            val code = json["errorCode"].int(response.code())
            val message = json["errorMessage"].string(bindString(R.string.common_error))
            return ApiException(code, message)
        }

        private fun getErrorMessage(
            @ApiExceptionType code: ApiExceptionCode,
            @StringRes defaultRes: Int
        ): String {
            return bindString(
                when (code) {
                    ERROR_CODE_NO_INTERNET -> R.string.common_error_no_internet
                    ERROR_CODE_NO_CONTRACT_CODE -> R.string.common_error_no_contract_code
                    else -> defaultRes
                }
            )
        }
    }

    val errorMessage
        get() = getErrorMessage(R.string.common_error_unknown)


    constructor(@ApiExceptionType code: ApiExceptionCode, cause: Throwable? = null) : this(
        code,
        getErrorMessage(code, R.string.common_error_unknown),
        cause
    )

    constructor(exception: ApiException) : this(
        exception.code,
        exception.throwableMessage,
        exception.cause
    )

    constructor(response: Response<*>?) : this(parse(response))

    constructor(cause: Throwable? = null) : this(
        ERROR_CODE_UNKNOWN,
        getErrorMessage(ERROR_CODE_UNKNOWN, R.string.common_error_unknown),
        cause
    )

    fun getErrorMessage(@StringRes defaultRes: Int): String {
        return getErrorMessage(code, defaultRes)
    }

    override fun toString(): String {
        return "ApiException = { code: $code, message: $errorMessage cause: $cause }"
    }
}