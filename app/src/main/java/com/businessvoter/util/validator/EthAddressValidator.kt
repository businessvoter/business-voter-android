package com.businessvoter.util.validator

import com.businessvoter.R
import com.businessvoter.util.extentions.bindString
import com.paytomat.eth.utils.WalletUtils

object EthAddressValidator {
    fun validate(text: String): String {
        return if (text.isBlank()) {
            ""
        } else if ((text.length > 1 && text.substring(
                IntRange(
                    0,
                    1
                )
            ) != "0x") || text.length <= 1
        ) {
            bindString(R.string.currency_address_validation_invalid)
        } else if (text.length != 42) {
            bindString(R.string.currency_address_validation_invalid_length)
        } else if (!WalletUtils.isValidAddress(text)) {
            bindString(R.string.currency_address_validation_invalid)
        } else {
            ""
        }
    }
}
