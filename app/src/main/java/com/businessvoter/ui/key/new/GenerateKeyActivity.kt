package com.businessvoter.ui.key.new

import android.app.AlertDialog
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.MenuItem
import androidx.lifecycle.ViewModelProvider
import com.businessvoter.R
import com.businessvoter.model.UserInfo
import com.businessvoter.ui.address.EnterTokenContractActivity
import com.businessvoter.ui.base.BaseVMActivity
import com.businessvoter.util.extentions.*
import com.google.android.material.snackbar.Snackbar
import kotlinx.android.synthetic.main.activity_generate_key.*

class GenerateKeyActivity : BaseVMActivity<GenerateKeyViewModel, GenerateKeyNavigator>() {
    companion object {
        fun getIntent(context: Context) = Intent(context, GenerateKeyActivity::class.java)
    }

    override fun initializeView() {
        setContentView(R.layout.activity_generate_key)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setSupportActionBar(toolbar)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean =
        when (item.itemId) {
            android.R.id.home -> onBackPressed().let { true }
            else -> super.onOptionsItemSelected(item)
        }

    override fun bindViews() {
        super.bindViews()
        generateKeyTxtPrivateKey.bindText(this, viewModel.generatedPrivateKey)
    }

    override fun bindClicksAndListeners() {
        super.bindClicksAndListeners()
        generateKeyImgCopy.setOnClickThrottled {
            viewModel.generatedPrivateKey.get().copyToClipboard(this)
            Snackbar.make(
                content,
                bindString(R.string.generate_pk_copy_success),
                Snackbar.LENGTH_SHORT
            ).show()
        }
        generateKeyBtnGenerate.setOnClickThrottled {
            viewModel.onGenerateClick()
        }
        generateKeyBtnContinue.setOnClickThrottled {
            viewModel.onContinueClick()
        }
    }

    override fun provideViewModel(): GenerateKeyViewModel =
        ViewModelProvider(this).get(GenerateKeyViewModel::class.java)

    override fun provideNavigator(): GenerateKeyNavigator = object :
        GenerateKeyNavigator {
        override fun showError(errorText: String) {
            AlertDialog.Builder(this@GenerateKeyActivity)
                .setTitle(R.string.common_error)
                .setMessage(errorText)
                .setPositiveButton(R.string.common_ok, null)
                .show()
        }

        override fun goToTokenAddressInput(userInfo: UserInfo) {
            startActivity(EnterTokenContractActivity.getIntent(this@GenerateKeyActivity, userInfo))
        }
    }

}