package com.businessvoter.db.dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.businessvoter.BusinessVoterApp
import com.businessvoter.model.db.Poll

@Dao
abstract class PollDao {

    companion object {
        val db: PollDao by lazy { BusinessVoterApp.db.pollDao() }
    }

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract fun insertPoll(model: Poll)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract fun insertPolls(models: List<Poll>)

    @Query("SELECT * FROM poll")
    abstract fun getPolls(): LiveData<List<Poll>>

    @Query("SELECT * FROM poll WHERE id=:id")
    abstract fun getPollById(id: Long): LiveData<Poll>
}