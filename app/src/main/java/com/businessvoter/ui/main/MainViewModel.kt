package com.businessvoter.ui.main

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.businessvoter.db.dao.AnswerDao
import com.businessvoter.db.dao.PollDao
import com.businessvoter.model.db.Answer
import com.businessvoter.model.db.Poll
import com.businessvoter.model.ui.PollUI
import com.businessvoter.prefs.UserPrefs
import com.businessvoter.repository.UserRepository.getUserInfo
import com.businessvoter.ui.base.BaseViewModel
import com.businessvoter.ui.base.empty.EmptyNavigator
import com.businessvoter.util.extentions.JobBuilder
import com.businessvoter.util.extentions.map
import com.businessvoter.util.extentions.set
import com.businessvoter.util.extentions.zip
import com.businessvoter.util.formatAmount
import com.businessvoter.util.toBigDecimalSafe
import java.math.BigDecimal

class MainViewModel : BaseViewModel<EmptyNavigator>() {
    val isRefreshing: MutableLiveData<Boolean> = MutableLiveData(false)

    val balance: LiveData<String> = UserPrefs.userTokenBalanceLiveData.map {
        formatAmount(it)
    }

    val staked: LiveData<String> = UserPrefs.userStakedLiveData.map {
        formatAmount(it)
    }

    private val userStakedPolls: LiveData<List<Poll>> = PollDao.db.getPolls().map {
        it.filter {
            it.userStaked.toBigDecimalSafe() > BigDecimal.ZERO
        }
    }

    private val userAnswers: LiveData<List<Answer>> = AnswerDao.db.getAnswers().map {
        it.filter {
            it.userStaked.toBigDecimalSafe() > BigDecimal.ZERO
        }
    }

    val models: LiveData<List<PollUI>> =
        userStakedPolls.zip(userAnswers).map { (userStakedPolls, userAnswers) ->
            val list: MutableList<PollUI> = mutableListOf()

            userStakedPolls.forEach { poll ->
                val answer: Answer? = userAnswers.firstOrNull { it.pollId == poll.id }
                if (answer != null) {
                    list.add(PollUI(poll, answer))
                }
            }

            return@map list
        }

    val isRecyclerVisible: LiveData<Boolean> = models.map { it.isNotEmpty() }

    val isEmptyViewVisible: LiveData<Boolean> = models.map { it.isEmpty() }

    fun onRefresh() {
        isRefreshing.set(true)
        JobBuilder {
            getUserInfo()
        }.finally {
            isRefreshing.set(false)
        }.runSafe(jobComposite)
    }
}