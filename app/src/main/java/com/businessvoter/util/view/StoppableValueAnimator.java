package com.businessvoter.util.view;

import android.animation.Animator;
import android.animation.TimeInterpolator;
import android.animation.ValueAnimator;

import androidx.annotation.NonNull;

import java.util.ArrayList;
import java.util.List;

public class StoppableValueAnimator {

    private class StoppableAnimatorUpdateListener implements ValueAnimator.AnimatorUpdateListener {
        @Override
        public void onAnimationUpdate(ValueAnimator animation) {
            if (!stopped)
                for (ValueAnimator.AnimatorUpdateListener listener : updateListeners)
                    listener.onAnimationUpdate(animation);
        }
    }

    private class StoppableAnimatorListener implements ValueAnimator.AnimatorListener {
        @Override
        public void onAnimationStart(Animator animation) {
            if (!stopped)
                for (ValueAnimator.AnimatorListener listener : listeners)
                    listener.onAnimationStart(animation);
        }

        @Override
        public void onAnimationEnd(Animator animation) {
            if (stopped)
                return;
            for (ValueAnimator.AnimatorListener listener : listeners)
                listener.onAnimationEnd(animation);
            stop();
        }

        @Override
        public void onAnimationCancel(Animator animation) {
            if (stopped)
                return;
            for (ValueAnimator.AnimatorListener listener : listeners)
                listener.onAnimationCancel(animation);
            stop();
        }

        @Override
        public void onAnimationRepeat(Animator animation) {
            if (stopped)
                return;
            for (ValueAnimator.AnimatorListener listener : listeners)
                listener.onAnimationRepeat(animation);
        }
    }

    @NonNull
    private final ValueAnimator animator;
    @NonNull
    private List<Animator.AnimatorListener> listeners = new ArrayList<>();
    @NonNull
    private List<ValueAnimator.AnimatorUpdateListener> updateListeners = new ArrayList<>();
    @NonNull
    private StoppableAnimatorListener stoppableAnimatorListener;
    @NonNull
    private StoppableAnimatorUpdateListener stoppableAnimatorUpdateListener;

    private boolean stopped;

    public StoppableValueAnimator(@NonNull final ValueAnimator animator) {
        this.animator = animator;
        stoppableAnimatorListener = new StoppableAnimatorListener();
        stoppableAnimatorUpdateListener = new StoppableAnimatorUpdateListener();
    }

    public void addListener(Animator.AnimatorListener listener) {
        listeners.add(listener);
        if (listeners.size() == 1)
            animator.addListener(stoppableAnimatorListener);
    }

    public void removeListener(Animator.AnimatorListener listener) {
        listeners.remove(listener);
        if (listeners.isEmpty())
            animator.removeListener(stoppableAnimatorListener);
    }

    public void setInterpolator(TimeInterpolator value) {
        animator.setInterpolator(value);
    }

    public void addUpdateListener(ValueAnimator.AnimatorUpdateListener listener) {
        updateListeners.add(listener);
        if (updateListeners.size() == 1)
            animator.addUpdateListener(stoppableAnimatorUpdateListener);
    }

    public void removeUpdateListener(ValueAnimator.AnimatorUpdateListener listener) {
        updateListeners.remove(listener);
        if (updateListeners.isEmpty())
            animator.removeUpdateListener(stoppableAnimatorUpdateListener);
    }

    public void setDuration(long duration) {
        animator.setDuration(duration);
    }

    public void start() {
        animator.start();
    }

    public boolean isStopped() {
        return stopped;
    }

    public void end() {
        animator.end();
        stop();
    }

    public void stop() {
        if (stopped)
            return;
        stopped = true;
        animator.removeUpdateListener(stoppableAnimatorUpdateListener);
        animator.removeListener(stoppableAnimatorListener);
        listeners.clear();
        updateListeners.clear();
    }


    public boolean isRunning() {
        return animator.isRunning();
    }
}