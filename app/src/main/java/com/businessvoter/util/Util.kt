package com.businessvoter.util

import android.content.SharedPreferences
import androidx.lifecycle.MutableLiveData
import com.businessvoter.prefs.EncodedPrefs
import java.math.BigDecimal
import java.math.BigInteger

abstract class EncodedPrefsLiveData<T>(
    val prefs: EncodedPrefs,
    val key: String,
    val defaultValue: T
) : MutableLiveData<T>() {

    private val prefListener = SharedPreferences.OnSharedPreferenceChangeListener { _, key ->
        if (key != prefs.toEncodedKey(this.key)) return@OnSharedPreferenceChangeListener
        setValue(getPrefValue())
    }

    override fun onActive() {
        setValue(getPrefValue())
        prefs.getPref().registerOnSharedPreferenceChangeListener(prefListener)
        super.onActive()
    }

    override fun onInactive() {
        prefs.getPref().unregisterOnSharedPreferenceChangeListener(prefListener)
        super.onInactive()
    }

    override fun setValue(value: T) {
        if (this.value != value) super.setValue(value)
        if (value != getPrefValue()) writePrefValue(value)
    }

    abstract fun getPrefValue(): T

    abstract fun writePrefValue(value: T)
}

class StringEncodedPrefsLiveData(prefs: EncodedPrefs, key: String, defaultValue: String) :
    EncodedPrefsLiveData<String>(prefs, key, defaultValue) {
    override fun writePrefValue(value: String) = prefs.writeString(key, value)

    override fun getPrefValue(): String = prefs.readString(key, defaultValue)
}

class LongEncodedPrefsLiveData(prefs: EncodedPrefs, key: String, defaultValue: Long = 0) :
    EncodedPrefsLiveData<Long>(prefs, key, defaultValue) {
    override fun writePrefValue(value: Long) = prefs.writeLong(key, value)

    override fun getPrefValue(): Long = prefs.readLong(key, defaultValue)
}

fun String?.toBigIntegerSafe(defaultValue: BigInteger = BigInteger.ZERO) =
    this?.toBigIntegerOrNull() ?: defaultValue

fun String?.toBigDecimalSafe(defaultValue: BigDecimal = BigDecimal.ZERO) =
    this?.toBigDecimalOrNull() ?: defaultValue

fun String?.toDoubleSafe(defaultValue: Double = 0.0) =
    this?.toDoubleOrNull() ?: defaultValue

fun Double?.toBigDecimalSafe(defaultValue: BigDecimal = BigDecimal.ZERO) =
    this?.toBigDecimal() ?: defaultValue

fun String?.toLongSafe(defaultValue: Long = 0): Long =
    this?.toLongOrNull() ?: defaultValue

fun String?.toIntSafe(defaultValue: Int = 0): Int =
    this?.toIntOrNull() ?: defaultValue