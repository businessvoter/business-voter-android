package com.businessvoter.util

import android.util.Base64
import java.nio.charset.Charset

fun decodeToken(token: String): String {
    val tokenData: String = token.substringAfter('.').substringBefore('.')
    val decodedToken: String =
        Base64.decode(tokenData, Base64.NO_WRAP).toString(Charset.defaultCharset())
    if (decodedToken.isBlank() || decodedToken == token) throw IllegalArgumentException("Wrong JWT token format")
    return decodedToken
}