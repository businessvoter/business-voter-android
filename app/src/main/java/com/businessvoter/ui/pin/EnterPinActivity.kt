package com.businessvoter.ui.pin

import android.app.Activity
import android.app.AlertDialog
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.MenuItem
import androidx.lifecycle.ViewModelProvider
import com.businessvoter.R
import com.businessvoter.model.LoginInfo
import com.businessvoter.ui.base.BaseVMActivity
import com.businessvoter.ui.main.MainActivity
import com.businessvoter.util.extentions.*
import kotlinx.android.synthetic.main.activity_enter_pin.*

class EnterPinActivity : BaseVMActivity<EnterPinViewModel, EnterPinNavigator>() {
    companion object {
        private val KEY_LOGIN_INFO = "KEY_LOGIN_INFO"
        private val KEY_CONFIRMATION_TEXT = "KEY_CONFIRMATION_TEXT"

        fun getIntent(context: Context, loginInfo: LoginInfo) =
            Intent(context, EnterPinActivity::class.java)
                .putExtra(KEY_LOGIN_INFO, loginInfo)

        fun getIntent(context: Context, confirmationText: String) =
            Intent(context, EnterPinActivity::class.java)
                .putExtra(KEY_CONFIRMATION_TEXT, confirmationText)
    }

    override fun initializeView() {
        setContentView(R.layout.activity_enter_pin)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setSupportActionBar(toolbar)

        viewModel.loginInfo.set(intent.getParcelableExtra(KEY_LOGIN_INFO))
        viewModel.description.set(intent.getStringExtra(KEY_CONFIRMATION_TEXT) ?: "")
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean =
        when (item.itemId) {
            android.R.id.home -> onBackPressed().let { true }
            else -> super.onOptionsItemSelected(item)
        }

    override fun bindViews() {
        super.bindViews()
        enterPinProgress.bindProgress(this, viewModel.progressMode)
        enterPinTxtDescription.bindText(this, viewModel.description)
        enterPinTxtDescription.bindVisibility(this, viewModel.isDescriptionVisible)
        enterPinEditTextPin.bindTextChange(this, viewModel.pin)
        enterPinEditTextConfirmPin.bindTextChange(this, viewModel.pinConfirmation)
        enterPinEditTextConfirmPin.bindVisibility(this, viewModel.isPinConfirmationVisible)
        enterPinBtnAction.bindText(this, viewModel.buttonText)
    }

    override fun bindClicksAndListeners() {
        super.bindClicksAndListeners()
        enterPinBtnAction.setOnClickThrottled {
            viewModel.onBtnClick()
        }
    }

    override fun provideViewModel(): EnterPinViewModel =
        ViewModelProvider(this).get(EnterPinViewModel::class.java)

    override fun provideNavigator(): EnterPinNavigator =
        object : EnterPinNavigator {
            override fun goToMainScreen() {
                startActivity(MainActivity.getIntent(this@EnterPinActivity))
                finish()
            }

            override fun showError(errorText: String) {
                AlertDialog.Builder(this@EnterPinActivity)
                    .setTitle(R.string.common_error)
                    .setMessage(errorText)
                    .setPositiveButton(R.string.common_ok, null)
                    .show()
            }

            override fun goBackWithSuccess() {
                setResult(Activity.RESULT_OK)
                finish()
            }
        }
}