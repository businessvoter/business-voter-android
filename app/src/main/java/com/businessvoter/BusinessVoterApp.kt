package com.businessvoter

import android.app.Application
import com.businessvoter.db.Db

open class BusinessVoterApp : Application() {
    companion object {
        lateinit var app: BusinessVoterApp
        val db: Db by lazy { Db.newInstance() }
    }

    override fun onCreate() {
        super.onCreate()
        app = this
    }
}