package com.businessvoter.model.api

data class EthGasPriceResponse(val gasPrice: String)

data class EthNonceResponse(val nonce: Long)

data class EthPostTransactionRequestBody(val raw: String)

data class EthPostTransactionResponse(val txId: String)