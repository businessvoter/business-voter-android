package com.businessvoter.util.view

import android.content.Context
import android.graphics.Typeface
import android.util.AttributeSet
import android.util.TypedValue
import android.view.Gravity
import android.view.View
import android.widget.FrameLayout
import android.widget.TextView
import com.businessvoter.R
import com.businessvoter.util.extentions.bindColor
import com.businessvoter.util.extentions.dpToPx

class StatusBoard @JvmOverloads constructor(
    context: Context,
    attr: AttributeSet? = null,
    defStyleAttr: Int = 0
) : FrameLayout(context, attr, defStyleAttr) {

    val radius = dpToPx(16).toFloat()
    val duration = 300L

    var lastStatus: String = ""
    var lastView: View? = null

    private val animBag = mutableListOf<Anim>()

    fun updateStatus(status: String) {
        if (status == lastStatus) return
        lastStatus = status

        lastView?.let {
            it.animate()
                .alpha(0F)
                .translationY(-radius)
                .setDuration(duration)
                .withEndAction {
                    removeView(it)
                }.bag(animBag)
        }

        lastView = TextView(context).apply {
            layoutParams = FrameLayout.LayoutParams(
                FrameLayout.LayoutParams.WRAP_CONTENT,
                FrameLayout.LayoutParams.WRAP_CONTENT
            ).apply {
                setMargins(0, dpToPx(24), 0, dpToPx(24))
                gravity = Gravity.CENTER
            }
            setTextColor(bindColor(R.color.textColorPrimary))
            setTextSize(TypedValue.COMPLEX_UNIT_SP, 24F)
            typeface = Typeface.create("sans-serif", Typeface.BOLD)
            gravity = Gravity.CENTER

        }.also {
            it.alpha = 0F
            it.translationY = radius
            it.text = status
            addView(it)
            it.animate()
                .alpha(1F)
                .translationY(0F)
                .setDuration(duration)
                .bag(animBag)
        }
    }

    fun clear() {
        animBag.cancelAll()
        removeView(lastView ?: return)
    }

    override fun onDetachedFromWindow() {
        super.onDetachedFromWindow()
        animBag.cancelAll()
    }
}