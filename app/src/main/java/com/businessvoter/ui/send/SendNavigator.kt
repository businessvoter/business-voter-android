package com.businessvoter.ui.send

import com.businessvoter.ui.base.BaseNavigator

interface SendNavigator : BaseNavigator {
    fun showError(errorText: String)
    fun goToConfirmPin(confirmText: String)
    fun goToSuccess(successText: String, txId: String)
}