package com.businessvoter.ui.pin

import com.businessvoter.ui.base.BaseNavigator

interface EnterPinNavigator : BaseNavigator {
    fun goToMainScreen()
    fun goBackWithSuccess()
    fun showError(errorText: String)
}