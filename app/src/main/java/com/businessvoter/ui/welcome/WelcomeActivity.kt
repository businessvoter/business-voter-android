package com.businessvoter.ui.welcome

import android.os.Bundle
import com.businessvoter.R
import com.businessvoter.prefs.UserPrefs
import com.businessvoter.ui.base.BaseVMActivity
import com.businessvoter.ui.base.empty.EmptyNavigator
import com.businessvoter.ui.base.empty.EmptyViewModel
import com.businessvoter.ui.key.new.GenerateKeyActivity
import com.businessvoter.ui.key.restore.EnterKeyActivity
import com.businessvoter.ui.main.MainActivity
import com.businessvoter.util.extentions.setOnClickThrottled
import kotlinx.android.synthetic.main.activity_welcome.*

class WelcomeActivity : BaseVMActivity<EmptyViewModel<EmptyNavigator>, EmptyNavigator>() {
    override fun initializeView() {
        setContentView(R.layout.activity_welcome)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (UserPrefs.hasPin()) {
            startActivity(MainActivity.getIntent(this))
            finish()
        }
    }

    override fun bindClicksAndListeners() {
        super.bindClicksAndListeners()
        welcomeBtnNew.setOnClickThrottled {
            startActivity(GenerateKeyActivity.getIntent(this))
        }
        welcomeBtnRestore.setOnClickThrottled {
            startActivity(EnterKeyActivity.getIntent(this))
        }
    }

    override fun provideViewModel(): EmptyViewModel<EmptyNavigator> = EmptyViewModel()

    override fun provideNavigator(): EmptyNavigator = object : EmptyNavigator {}
}