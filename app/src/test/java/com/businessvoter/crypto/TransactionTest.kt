package com.businessvoter.crypto

import com.businessvoter.util.crypto.DataActionEncoder.encodeStakeData
import com.paytomat.eth.utils.Numeric
import org.bouncycastle.util.encoders.Hex
import org.junit.Assert
import org.junit.Test
import org.web3j.crypto.Credentials
import org.web3j.crypto.ECKeyPair
import org.web3j.crypto.RawTransaction
import org.web3j.crypto.TransactionEncoder
import java.math.BigInteger


class TransactionTest {
    @Test
    fun testTransactionSerialization() {
        val privateKey: ByteArray =
            Hex.decode("7330532f25298dd8047f839d41a77de3dbd04d7048cbfbffd03d6daf1246b4d8")
        val credentials: Credentials = Credentials.create(ECKeyPair.create(privateKey))

        val nonce: BigInteger = BigInteger.ZERO
        val value = BigInteger.valueOf(1)
        val gasLimit = BigInteger.valueOf(21000)
        val gasPrice = BigInteger.valueOf(1000000000)
        val chainId: Byte = 3
        val to = "0x4592d8f8d7b001e72cb26a73e4fa1806a51ac79d"

        val rawTransaction: RawTransaction =
            RawTransaction.createEtherTransaction(nonce, gasPrice, gasLimit, to, value)
        val signedMessage: ByteArray =
            TransactionEncoder.signMessage(rawTransaction, chainId, credentials)

        val hexMessage: String = Numeric.toHexString(signedMessage)
        Assert.assertEquals(
            "0xf86380843b9aca00825208944592d8f8d7b001e72cb26a73e4fa1806a51ac79d01802aa00d925d1795136c913e756faf73c693c74b9c9f23eb388d1141ac2dc1f33d621ca064b12418b9451c9b023f68173be034180ddf20a70e01b594a256f1ca0dc28564",
            hexMessage
        )
    }

    @Test
    fun testStakeTransactionSerialization() {
        val privateKey: ByteArray =
            Hex.decode("C0A9D6C5DF62C9EDA4A5A6241101CF9B30518CD5F68351FA682A730B22898728")
        val credentials: Credentials = Credentials.create(ECKeyPair.create(privateKey))

        val nonce: BigInteger = BigInteger.valueOf(64)
        val gasLimit = BigInteger.valueOf(800000)
        val gasPrice = BigInteger.valueOf(25000000)
        val chainId: Byte = 3
        val data = encodeStakeData(
            "0x39A29c6B886ceEbFfdD663163a3C11FF4DBd0A9F",
            BigInteger.valueOf(200),
            BigInteger.ONE,
            BigInteger.ONE
        )

        val rawTransaction: RawTransaction =
            RawTransaction.createTransaction(
                nonce,
                gasPrice,
                gasLimit,
                "0x39A29c6B886ceEbFfdD663163a3C11FF4DBd0A9F",
                BigInteger.ZERO,
                data
            )
        val signedMessage: ByteArray =
            TransactionEncoder.signMessage(rawTransaction, chainId, credentials)

        val hexMessage: String = Numeric.toHexString(signedMessage)
        System.out.println("hexMessage - $hexMessage")
    }

}