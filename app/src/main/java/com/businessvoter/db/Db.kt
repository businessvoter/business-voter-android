package com.businessvoter.db

import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.room.migration.Migration
import com.businessvoter.BusinessVoterApp.Companion.app
import com.businessvoter.db.dao.AnswerDao
import com.businessvoter.db.dao.PollDao
import com.businessvoter.model.db.Answer
import com.businessvoter.model.db.Poll

@Database(
    entities = [Answer::class, Poll::class],
    version = 1,
    exportSchema = false
)
abstract class Db : RoomDatabase() {
    companion object {

        private const val DB_NAME = "business_voter_db"

        fun newInstance(): Db {
            return Room
                .databaseBuilder(app, Db::class.java, DB_NAME)
                .allowMainThreadQueries()
                .fallbackToDestructiveMigration()
                .addMigrations(*MIGRATIONS)
                .build()
        }
    }

    abstract fun answerDao(): AnswerDao

    abstract fun pollDao(): PollDao
}

private val MIGRATIONS: Array<Migration> = arrayOf()
