package com.businessvoter.ui.main

import android.view.View
import com.businessvoter.model.ui.PollUI
import com.businessvoter.ui.base.BaseViewHolder
import com.businessvoter.util.extentions.bindColor
import com.businessvoter.util.formatAmount
import kotlinx.android.synthetic.main.item_poll_staked.view.*

class MainPollViewHolder(view: View) : BaseViewHolder<PollUI>(view) {
    override fun bind(model: PollUI) {
        itemView.apply {
            itemPollStakedFormattedStake.text = formatAmount(model.userStaked)
            itemPollStakedQuestion.text = model.question
            itemPollStakedAnswer.text = model.answer
            itemPollStakedStatus.apply {
                text = model.getStatusText()
                setTextColor(bindColor(model.getTextColor()))
            }
            setOnClickListener { clickListener?.onClick(adapterPosition, model) }
        }
    }
}