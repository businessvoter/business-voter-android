package com.businessvoter.util

import kotlinx.coroutines.Job
import kotlinx.coroutines.cancelChildren
import java.util.concurrent.CancellationException
import java.util.concurrent.atomic.AtomicBoolean

class JobComposite {

    private val jobs: MutableList<Job> = mutableListOf()
    private val isCanceled: AtomicBoolean = AtomicBoolean(false)

    fun cancel() {
        if (isCanceled.getAndSet(true)) return
        synchronized(this) {
            trim()
            cancelAll()
            trim()
        }
    }

    private fun cancelAll() {
        if (jobs.isEmpty()) return
        for (job in jobs) {
            try {
                job.cancelChildren()
            } catch (e: CancellationException) {
                e.printStackTrace()
            }
        }
    }

    fun isCanceled(): Boolean = isCanceled.get()

    fun add(job: Job): Boolean = synchronized(this) {
        trim()
        if (!isCanceled()) {
            jobs.add(job)
            true
        } else {
            job.cancel()
            false
        }
    }

    fun size(): Int = synchronized(this) {
        if (isCanceled()) return 0
        else jobs.size
    }

    private fun trim() {
        synchronized(this) { jobs.removeAll { it.isCancelled || it.isCompleted } }
    }

}