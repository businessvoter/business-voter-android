package com.businessvoter.util.extentions

import android.content.Context
import android.content.SharedPreferences
import android.util.TypedValue
import androidx.annotation.ColorRes
import androidx.annotation.StringRes
import androidx.core.content.ContextCompat
import com.businessvoter.BusinessVoterApp

fun bindString(@StringRes res: Int): String = BusinessVoterApp.app.getString(res)

fun bindString(@StringRes res: Int, vararg args: Any): String =
    BusinessVoterApp.app.getString(res, *args)

fun bindColor(@ColorRes res: Int, context: Context? = null): Int =
    ContextCompat.getColor(context ?: BusinessVoterApp.app, res)

fun SharedPreferences.readString(key: String, defaultValue: String): String =
    getString(key, defaultValue) ?: defaultValue

fun dpToPx(dp: Int): Int = dpToPx(dp.toFloat())

fun dpToPx(dp: Float): Int = Math.round(dp * BusinessVoterApp.app.resources.displayMetrics.density)

fun spToPx(sp: Int): Float = TypedValue.applyDimension(
    TypedValue.COMPLEX_UNIT_SP,
    sp.toFloat(), BusinessVoterApp.app.resources.displayMetrics
)