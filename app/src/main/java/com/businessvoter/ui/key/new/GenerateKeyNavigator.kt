package com.businessvoter.ui.key.new

import com.businessvoter.model.UserInfo
import com.businessvoter.ui.base.BaseNavigator

interface GenerateKeyNavigator : BaseNavigator {
    fun showError(errorText: String)
    fun goToTokenAddressInput(userInfo: UserInfo)
}