package com.businessvoter.ui.base

import android.annotation.SuppressLint
import android.content.pm.PackageManager
import android.os.Bundle
import android.view.ViewPropertyAnimator
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import com.businessvoter.util.ClickThrottler
import java.util.*

@SuppressLint("Registered")
abstract class BaseVMActivity<V : BaseViewModel<N>, N : BaseNavigator> : AppCompatActivity() {

    protected lateinit var viewModel: V
    protected lateinit var navigator: N

    protected var animatorPool: MutableList<ViewPropertyAnimator> = LinkedList()
    protected val clickThrottler = ClickThrottler()

    abstract fun initializeView()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initializeView()
        viewModel = provideViewModel()
        navigator = provideNavigator()
        viewModel.navigator = navigator
        bindViews()
        bindClicksAndListeners()
    }

    open fun bindViews() {}

    open fun bindClicksAndListeners() {}

    override fun onDestroy() {
        super.onDestroy()
        viewModel.navigator = null
        animatorPool.forEach { it.cancel() }
    }

    protected open fun hasPermissions(vararg permissions: String): Boolean =
        permissions.all {
            ActivityCompat.checkSelfPermission(
                this,
                it
            ) == PackageManager.PERMISSION_GRANTED
        }

    protected open fun shouldShowRationaleDialog(vararg permissions: String): Boolean =
        !hasPermissions(*permissions) &&
                permissions.any { ActivityCompat.shouldShowRequestPermissionRationale(this, it) }

    protected open fun askPermissions(permissionCode: Int, vararg permissions: String) {
        ActivityCompat.requestPermissions(this, permissions, permissionCode)
    }

    abstract fun provideViewModel(): V
    abstract fun provideNavigator(): N
}