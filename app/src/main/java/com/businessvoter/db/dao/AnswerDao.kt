package com.businessvoter.db.dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.businessvoter.BusinessVoterApp
import com.businessvoter.model.db.Answer

@Dao
abstract class AnswerDao {

    companion object {
        val db: AnswerDao by lazy { BusinessVoterApp.db.answerDao() }
    }

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract fun insertAnswers(models: List<Answer>)

    @Query("SELECT * FROM answer")
    abstract fun getAnswers(): LiveData<List<Answer>>

    @Query("SELECT * FROM answer WHERE pollId=:pollId")
    abstract fun getAnswersByPollId(pollId: Long): LiveData<List<Answer>>

    @Query("SELECT * FROM answer WHERE pollId=:pollId AND id=:id")
    abstract fun getAnswerById(pollId: Long, id: Long): Answer?
}