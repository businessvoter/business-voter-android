package com.businessvoter.util.extentions

fun Long.msToSec(): Long = this / 1000L

fun Long.secToMs(): Long = this * 1000L